import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem, Button} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

export default class RegisterScreen extends Component{
	render(){
		return(
			<Container>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}}onPress={() => this.props.navigation.navigate('Payment')}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>Terms and Condition</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content style={{paddingHorizontal: 10}}>
					<View>
						<Text style={styles.textTitle}>Terms and Conditions</Text>
						<Text style={styles.textNorm}>This (mobile) website, hotelwaze.com, and all services and products that we offer, shall be governed by these Terms and Conditions and shall apply to any and all pages, subpages and links attributed to it. As such, these Terms and Conditions are subject to change and may be altered, amended or otherwise modified without prior approval or notice to any third person or user thereof. By accessing, browsing or using this (mobile) website, and/or by placing or completing a reservation, you acknowledge and unequivocally accede to have read, understood and agreed to the totality of these Terms and Conditions
						</Text>
						<Text style={styles.textNorm}>Primeraka LLC owns and operates the entire content and infrastructure of this (mobile) website and is intended and provided for your personal and non-commercial use.
						</Text>
						<Text style={styles.textNorm}>Scope of services</Text>
						<Text style={styles.textNorm}>Our business includes providing an online platform for bridging and connecting customers and tourism establishments such as, but not limited to, hotels, motels, theme parks, travel agencies, theaters, concerts, and any other travel and tourism-related products and services (“service providers” for brevity), by advertising and offering reservation and online booking services. You are entering into a legally binding contract with our service providers for any and all reservations and bookings made through this (mobile) website. Hotelwaze.com acts exclusively as an intermediary between the customer and the service provider by sending and transmitting information to facilitate transactions.
						</Text>
						<Text style={styles.textNorm}>Hotelwaze.com does not own or produce the information provided by the service providers and are exclusively and solely under the latter’s effective control and discretion. The service providers are esponsible for any alteration, modification, amendment or revision of any information regarding their respective establishments, including but not limited to, rates, availability, location, quality and all other information. We take pride in providing you with the best quality service, as such we treat with utmost diligence and care to ascertain the veracity, accuracy and correctness of all information coursed through our (mobile) website. However, Primeraka LLC shall not be liable for any misinformation, wrong information or misleading information for any content displayed through Hotelwaze.com, whether through typographical errors or otherwise.
						</Text>
						<Text style={styles.textNorm}>Privacy</Text>
						<Text style={styles.textNorm}>Hotelwaze.com adheres to internet and information privacy. Hence, all dealings, reservations, transactions and information coursed through our (mobile) website shall be considered confidential information and shall be subject to existing laws, rules and regulations of the United States of America, relative to confidentiality and privacy.
						</Text>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({	
	container:{
		padding: 10,
	},
	header:{
		alignItems: 'center',
		backgroundColor: '#fff',
		elevation: 0,
	},
	textTitle:{
		fontSize: 12,
		paddingBottom: 10,
	},
	textNorm:{
		fontSize: 10,
		paddingBottom: 10
	}

});
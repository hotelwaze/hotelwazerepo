import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem, Button} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

export default class RegisterScreen extends Component{
	render(){
		return(
			<Container>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}}onPress={() => this.props.navigation.navigate('Home')}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>Register</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content style={{paddingHorizontal: 10, paddingTop: 15}}>
					<View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>First Name</Text>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Last Name</Text>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Email Address</Text>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Contact Number</Text>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Password</Text>
							<TextInput style={styles.input} editable maxLen ght="100" />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Confirm Password</Text>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
						<View style={{alignItems: 'flex-end'}}>
							<Button style={{width: 105, height: 30, borderRadius: 15, backgroundColor: '#006CFF'}} onPress={()=>this.props.navigation.navigate('Payment')}>
								<Text style={{textAlign: 'center', width: '100%', color: '#fff', fontSize: 10}}>Next</Text>
							</Button>
						</View>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({	
	container:{
		padding: 10,
	},
	header:{
		alignItems: 'center',
		backgroundColor: '#fff',
		elevation: 0,
	},
	boxContainer:{
		flex: 1,
		borderRadius: 5,
		paddingHorizontal: 10,
		paddingVertical: 20,
		margin: 10,
		backgroundColor: '#fff',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
	},
	title:{
		fontSize: 10,
		color: '#a5a5a5',
		paddingBottom: 5,
	},
	searchInput:{
		backgroundColor: '#f1f1f1',
		position: 'absolute', 
		top: 0,
		left: 0,
		right: 0,
	},
	input:{
		height: 30,
        padding: 10,
        fontSize: 18,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
	},
	list:{
		paddingTop: 15
	},
	listItem:{
		paddingHorizontal: 15,
		paddingVertical: 0,
		height: 30,
		backgroundColor: '#fff',
		borderRadius: 15,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3
	},
	listItemContent:{
		alignItems: 'center'
	},
	listItemText:{
		fontSize: 14,
		color: '#A5A5A5'
	},
	listItemIcon:{
		fontSize: 18,
		color: '#A5A5A5'
	},
	
});
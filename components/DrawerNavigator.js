
import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView, Dimensions, Image, TouchableHighlight, Animated, AsyncStorage, Alert} from 'react-native';
import {Row, Col, Icons, Content, List, ListItem, Button} from 'native-base';
import SafeAreaView from 'react-native-safe-area-view';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPowerOff, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import {createDrawerNavigator, DrawerNavigatorItems} from 'react-navigation-drawer';
import {createAppContainer, NavigationActions} from 'react-navigation';

import HomeScreen from './HomeScreen';
import SearchScreen from './SearchScreen';
import SingleSearchItemScreen from './SingleSearchItemScreen';
import SelectCountryScreen from './SelectCountryScreen';
import SelectStateScreen from './SelectStateScreen';
import SelectCityScreen from './SelectCityScreen';
import AccountScreen from './AccountScreen';
import ContactUsScreen from './ContactUsScreen';

import SignInScreen from './SignInScreen';
import RegisterScreen from './RegisterScreen';
import PaymentScreen from './PaymentScreen';
import Terms from './Terms';



export default class DrawerNavigator extends Component{

	render(){
		return (<AppContainer />);
	}
}

const CustomDrawerComponents = (props) => (
	<ScrollView>
		<SafeAreaView style={styles.container}>
			<View style={styles.contentWrapper}>
				<Row>
					<View style={styles.letterBox}>
						<Text style={styles.firtsLetter}>N</Text>
					</View>
					<Col style={styles.details}>
						<Text style={styles.name}>Noel D. Bang-asan</Text>
						<Text style={styles.email}>Email: nbangasan@gmail.com</Text>
						<Text style={styles.subscriptionDetails}>Subscription: Jan 2, 2019 - Jan 2, 2020</Text>
					</Col>
					<View style={styles.offSign}>
						<Button transparent onPress={this._handleLogout}>
							<FontAwesomeIcon style={styles.icon} icon={ faPowerOff } />
						</Button>
					</View>
				</Row>
				<Row>
					<View style={{flex: 1, paddingTop: 20}}>
						<Text>Account</Text>
						<View style={styles.list}>
							<Button transparent style={styles.listItem} onPress={() => navigation.navigate('Home')}>
								<Row style={styles.listItemContent}>
									<Col>
										<Text style={styles.listItemText}>Change Email</Text>
									</Col>
									<View>
										<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
									</View>
								</Row>
							</Button>
						</View>
						<View style={styles.list}>
							<Button transparent style={styles.listItem}>
								<Row style={styles.listItemContent}>
									<Col>
										<Text style={styles.listItemText}>Change Password</Text>
									</Col>
									<View>
										<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
									</View>
								</Row>
							</Button>
						</View>
					</View>
				</Row>
				<Row>
					<View style={{flex: 1, paddingTop: 20}}>
						<Text>Subscription</Text>
						<View style={styles.list}>
							<Button transparent style={styles.listItem}>
								<Row style={styles.listItemContent}>
									<Col>
										<Text style={styles.listItemText}>Reactivate Subscription</Text>
									</Col>
									<View>
										<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
									</View>
								</Row>
							</Button>
						</View>
						<View style={styles.list}>
							<Button transparent style={styles.listItem}>
								<Row style={styles.listItemContent}>
									<Col>
										<Text style={styles.listItemText}>Cancel Subscription</Text>
									</Col>
									<View>
										<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
									</View>
								</Row>
							</Button>
						</View>
						<View style={styles.list}>
							<Button transparent style={styles.listItem}>
								<Row style={styles.listItemContent}>
									<Col>
										<Text style={styles.listItemText}>Transaction History</Text>
									</Col>
									<View>
										<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
									</View>
								</Row>
							</Button>
						</View>
					</View>
				</Row>
			</View>
		</SafeAreaView>
	</ScrollView>
);


const MyDrawerNavigator = createDrawerNavigator({
		Home : {screen: HomeScreen},
		Search: {screen: SearchScreen},
		SingleSearchItem: {screen:SingleSearchItemScreen},
		SelectCountry: {screen: SelectCountryScreen},
		SelectState: {screen: SelectStateScreen},
		SelectCity: {screen: SelectCityScreen},
		Account: {screen: AccountScreen},
		ContactUs: {screen:ContactUsScreen},
		SignIn: {screen:SignInScreen},
		Register: {screen: RegisterScreen},
		Payment: {screen: PaymentScreen},
		Terms: {screen: Terms},
	},
	{
		initialRouteName: 'Home',
		drawerWidth: 300,
		drawerPosition: 'left',
		contentComponent: CustomDrawerComponents
	}
);

const AppContainer = createAppContainer(MyDrawerNavigator);

_handleLogout = async() => {
	try {
	  await AsyncStorage.removeItem('_signInSession');
	  const value = await AsyncStorage.getItem('_signInSession');
	  global.token = value;	  
	} catch (error) {
	  console.log("Error resetting data"+error);
	}
};




const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentWrapper:{
  	padding: 10,
  	flex: 1,
  	//backgroundColor: 'green'
  },
  letterBox:{
  	width: 46,
  	height: 46,
  	backgroundColor: '#003580',
  	borderRadius: 46,
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  details:{
  	paddingLeft: 10
  },
  firtsLetter:{
  	color: '#fff',
  	fontSize: 21,
  	fontWeight: 'bold'
  },
  name:{
  	fontSize: 18,
  	fontWeight: 'bold',
  	color: '#003580'
  },
  email:{
  	fontSize: 10,
  	color:'#A5A5A5'
  },
  subscriptionDetails:{
  	fontSize: 10
  },
  offSign:{
  	height: 30,
  	width: 30,
  	backgroundColor: '#006CFF',
  	justifyContent: 'center',
  	alignItems: 'center',
  	borderRadius: 30,
  },
  icon:{
  	color: '#fff',
  	fontSize: 20
  },
  list:{
  	paddingTop: 15
  },
  listItem:{
  	paddingHorizontal: 15,
  	paddingVertical: 0,
  	height: 30,
  	backgroundColor: '#fff',
  	borderRadius: 15,
  	shadowColor: "#000",
	shadowOffset: {
	 width: 0,
	 height: 1,
	},
	shadowOpacity: 0.22,
	shadowRadius: 2.22,
	elevation: 3
  },
  listItemContent:{
  	alignItems: 'center'
  },
  listItemText:{
  	fontSize: 14,
  	color: '#A5A5A5'
  },
  listItemIcon:{
  	fontSize: 18,
  	color: '#A5A5A5'
  }

});
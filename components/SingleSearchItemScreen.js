import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, Dimensions, TouchableOpacity, Linking, Platform, NetInfo} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMapMarkerAlt, faPhone, faGlobe, faArrowLeft, faEnvelope} from '@fortawesome/free-solid-svg-icons';
import MapView,  { PROVIDER_GOOGLE } from 'react-native-maps';

import { Audio } from 'expo-av';

import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo-ads-admob';

const { width, height } = Dimensions.get('window');
const audioSound = {
    one: require('../audio/officialwebsite.mp3'),
}

export default class SingleSearchItem extends Component{

	constructor(props) {
		super(props);
		this.state =  {
			region: {
				latitude: 0,
				longitude: 0,
				latitudeDelta: 0,
				longitudeDelta: 0,
			},
			coordinate: {
				latitude: 0,
				longitude: 0,
			}
		}
	}

	handlePlay = async (_play, website) => {
        if(audiox == 1){
            const soundObject = new Audio.Sound();
            try {
                let sourcex  = audioSound[_play]
                await soundObject.loadAsync(sourcex);
                await soundObject
                    .playAsync()
                    .then(async playbackStatus => {
                        setTimeout(() => {
                            Linking.openURL(website);
                        }, 3000)
                       
                    })
                    .catch(error => {
                        console.log(error)
                    })
            } catch (error) {
                console.log(error)
            }
        }else{
            Linking.openURL(website);
        }
    }

	componentWillMount(){
		this.CheckConnectivity();
	}

	CheckConnectivity = () => {
		// For Android devices
		if (Platform.OS === "android") {
			NetInfo.isConnected.fetch().then(isConnected => {
				if (isConnected) {
					this.fetchCoordinates();
				} else {					
				    this.props.navigation.navigate('NoConnection');
				}
			});
		} else {
			// For iOS devices
			NetInfo.isConnected.addEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
			);
		}
	};

	handleFirstConnectivityChange = isConnected => {
		NetInfo.isConnected.removeEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
		);

		if (isConnected === false) {
			this.props.navigation.navigate('NoConnection');
		} else {
			this.fetchCoordinates();
		}
	};

	fetchCoordinates = () =>{
		const ASPECT_RATIO = width / height;
		const LATITUDE = latitudex;
		const LONGITUDE = longitudex;
		const LATITUDE_DELTA = 0.01;
		const LONGITUDE_DELTA = 0.01;
		this.setState({
			region: {
				latitude: LATITUDE,
				longitude: LONGITUDE,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA,
			},
			coordinate: {
				latitude: LATITUDE,
				longitude: LONGITUDE,
			}
		})
	};

	passScreenName = (name, screenName, hotelID) =>{
    	global.hotelNamex = name;
    	global.screenNamex = screenName;
    	global.hotelIDx = hotelID;
    	this.props.navigation.navigate('Enquiry');
    };


	render(){
		
		return(
			<Container>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Search')}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>{hotelNamex.replace('\r\n', '')}</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content>
					<View style={styles.searchContent}>
						<MapView
							style={{width: '100%', height: 350}}
							provider={PROVIDER_GOOGLE}
							initialRegion={this.state.region}
						>
							<MapView.Marker coordinate={this.state.coordinate}/>
        				</MapView>
					</View>
					{/**Google Adds**/}
					<View style={{width: '100%', display:'flex', alignItems:'center', justifyContent:'center', paddingBottom: 5}}>
						<AdMobBanner
						  	bannerSize="banner"
						  	adUnitID="ca-app-pub-8722731472004532/9999144645" // Test ID, Replace with your-admob-unit-id
						  	testDeviceID="EMULATOR"
						  	servePersonalizedAds // true or false
						  	onDidFailToReceiveAdWithError={this.bannerError} 
						/>
					</View>
					<View style={{paddingTop: 10}}>
						<View style={styles.searchContentInfo}>
							<TouchableOpacity onPress={()=>{this.handlePlay('one',hotelWebsitex)}}>
								<Text style={styles.searchResultTitle}>{hotelNamex.replace('\r\n', '')}</Text>
							</TouchableOpacity>
							<Row style={styles.searcResultTitleContainer}>
								<FontAwesomeIcon style={styles.searhResultInfoIcon} icon={faMapMarkerAlt} />
								<Text style={styles.searchResultInfoTitle}>{hotelAddress1x.replace('\r\n', '')}</Text>
							</Row>
							<Row style={{paddingTop: 15, paddingBottom: 5}}>
                                <Col style={{paddingRight: 15}}>
                                    <TouchableOpacity style={styles.contactInfo} onPress={()=>{Linking.openURL(`tel:${hotelPhonex}`)}}>
                                        <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                            <View style={{padding: 15}}>
                                                <View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
                                                    <View style={{paddingBottom: 5}}>
                                                        <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faPhone} />
                                                    </View>
                                                    <Text style={styles.searchResultInfoTitlex}>Contact</Text>
                                                    <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </Col>
                                <Col style={{paddingRight: 15}}>
                                    {permissionx == 1 ?
                                        <TouchableOpacity style={styles.contactInfo}
                                            onPress={() =>
		                                        this.passScreenName(
		                                            hotelNamex,
		                                            'singleSearchScreenx',
		                                            hotelIDx
		                                        )
                                        }>
                                            <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                                <View style={{padding: 15}}>
                                                    <View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
                                                        <View style={{paddingBottom: 5}}>
                                                            <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faEnvelope} />
                                                        </View>
                                                        <Text style={styles.searchResultInfoTitlex}>Enquire</Text>
                                                        <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                       
                                        : 
                                        
                                        <TouchableOpacity style={styles.contactInfo} disabled={true}>
                                            <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                                <View style={{padding: 15}}>
                                                    <View style={{display:'flex', alignItems:'center',justifyContent:'center',backgroundColor: '#fff', opacity:0.4}}>
                                                        <View style={{paddingBottom: 5}}>
                                                            <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faEnvelope} />
                                                        </View>
                                                        <Text style={styles.searchResultInfoTitlex}>Enquire</Text>
                                                        <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    }
                                </Col>
                                <Col style={{paddingRight: 15}}>
                                    <TouchableOpacity style={styles.contactInfo} onPress={()=>{this.handlePlay('one',hotelWebsitex)}}>
                                        <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                            <View style={{padding: 15}}>
                                                <View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
                                                    <View style={{paddingBottom: 5}}>
                                                        <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faGlobe} />
                                                    </View>
                                                    <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                    <Text style={styles.searchResultInfoTitlex}>Website</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </Col>
                                <Col>
                                    
                                </Col>
                            </Row>
						</View>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	contactInfo:{
    	display:'flex', 
    	alignItems:'center', 
    	justifyContent:'center',
    	shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height:2,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    searchResultInfoTitlex:{
    	textAlign: 'center',
        color: '#003580',
        fontSize: 8,
        paddingLeft: 5,
    },
    searhResultInfoIconx:{
        color: '#003580',
        fontSize: 31,
    },	
	container:{
		padding: 10,
	},
	header:{
		alignItems: 'center',
		backgroundColor: '#fff',
		elevation: 0,
	},
	searchContent:{
		flex: 1,
		borderRadius: 5,
		marginHorizontal: 10,
		marginTop: 10,
		marginBottom: 0,
		backgroundColor: '#fff',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
	},
	searchContentInfo:{
		flex: 1,
		borderRadius: 5,
		padding: 10,
		margin: 10,
		backgroundColor: '#fff',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
	},
	searchResultTitle:{
		color: '#003580',
		fontSize: 18,
		fontWeight: 'bold',
	},
	searcResultTitleContainer:{
		paddingTop: 5,
	},
	searhResultInfoIcon:{
		color: '#003580',
		fontSize: 10,
	},
	searchResultInfoTitle:{
		color: '#003580',
		fontSize: 12,
		paddingLeft: 5,
	}
	
});
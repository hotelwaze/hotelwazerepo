
import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView, Dimensions, Image, TouchableHighlight, Animated, AsyncStorage, Alert} from 'react-native';
import {Row, Col, Icons, Content, List, ListItem, Button} from 'native-base';
import SafeAreaView from 'react-native-safe-area-view';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faPowerOff, faArrowRight } from '@fortawesome/free-solid-svg-icons';
import {NavigationActions} from 'react-navigation';

export default class DrawerItem extends Component{
	

	navigateToScreen = ( route ) =>(
        () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
    });

    _handleLogout = async() => {
		try {
		  await AsyncStorage.removeItem('_signInSession');
		  const value = await AsyncStorage.getItem('_signInSession');
		  global.tokenx = '';
		  this._out(); 
		} catch (error) {
		  console.log("Error resetting data"+error);
		}
	};

	_out = () =>{
		this.props.navigation.navigate('SignIn');
	}




	render(){
		if(tokenx == ''){
			return(
				<ScrollView>
					<SafeAreaView style={styles.container}>
						<View style={styles.contentWrapper}>
							<Row>
								<View style={styles.letterBox}>
									<Text style={styles.firtsLetter}>G</Text>
								</View>
								<Col style={styles.details}>
									<Text style={styles.name}>Hello, Guest</Text>
									<Text style={styles.email}>Email: N/A</Text>
									<Text style={styles.subscriptionDetails}>Subscription: N/A</Text>
								</Col>
							</Row>
							<Row>
								<View style={{flex: 1, paddingTop: 20}}>
									<View style={styles.list}>
										<Button transparent style={styles.listItem} onPress={this.navigateToScreen('SignIn')}>
											<Row style={styles.listItemContent}>
												<Col>
													<Text style={styles.listItemText}>Sign In</Text>
												</Col>
												<View>
													<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
												</View>
											</Row>
										</Button>
									</View>
									<View style={styles.list}>
										<Button transparent style={styles.listItem} onPress={this.navigateToScreen('Register')}>
											<Row style={styles.listItemContent}>
												<Col>
													<Text style={styles.listItemText}>Register</Text>
												</Col>
												<View>
													<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
												</View>
											</Row>
										</Button>
									</View>
								</View>
							</Row>
						</View>
					</SafeAreaView>
				</ScrollView>
			);
		}else{
			return(
				<ScrollView>
					<SafeAreaView style={styles.container}>
						<View style={styles.contentWrapper}>
							<Row>
								<View style={styles.letterBox}>
									<Text style={styles.firtsLetter}>N</Text>
								</View>
								<Col style={styles.details}>
									<Text style={styles.name}>{firstNamex} {lastNamex}</Text>
									<Text style={styles.email}>Email: {emailx}</Text>
									<Text style={styles.subscriptionDetails}>Subscription: Jan 2, 2019 - Jan 2, 2020</Text>
								</Col>
								<View>
									<Button transparent style={styles.offSign} onPress={this._handleLogout }>
										<FontAwesomeIcon style={styles.icon} icon={ faPowerOff } />
									</Button>
								</View>
							</Row>
							<Row>
								<View style={{flex: 1, paddingTop: 20}}>
									<Text>Account</Text>
									<View style={styles.list}>
										<Button transparent style={styles.listItem} onPress={this.navigateToScreen('AccountChangeEmail')}>
											<Row style={styles.listItemContent}>
												<Col>
													<Text style={styles.listItemText}>Change Email</Text>
												</Col>
												<View>
													<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
												</View>
											</Row>
										</Button>
									</View>
									<View style={styles.list}>
										<Button transparent style={styles.listItem} onPress={this.navigateToScreen('AccountChangePassword')}>
											<Row style={styles.listItemContent}>
												<Col>
													<Text style={styles.listItemText}>Change Password</Text>
												</Col>
												<View>
													<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
												</View>
											</Row>
										</Button>
									</View>
								</View>
							</Row>
							<Row>
								<View style={{flex: 1, paddingTop: 20}}>
									<Text>Subscription</Text>
									<View style={styles.list}>
										<Button transparent style={styles.listItem} onPress={this.navigateToScreen('Reactivation')}>
											<Row style={styles.listItemContent}>
												<Col>
													<Text style={styles.listItemText}>Reactivate Subscription</Text>
												</Col>
												<View>
													<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
												</View>
											</Row>
										</Button>
									</View>
									<View style={styles.list}>
										<Button transparent style={styles.listItem} onPress={this.navigateToScreen('CancelSubscription')}>
											<Row style={styles.listItemContent}>
												<Col>
													<Text style={styles.listItemText}>Cancel Subscription</Text>
												</Col>
												<View>
													<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
												</View>
											</Row>
										</Button>
									</View>
									<View style={styles.list}>
										<Button transparent style={styles.listItem} onPress={this.navigateToScreen('TransactionHistory')}>
											<Row style={styles.listItemContent}>
												<Col>
													<Text style={styles.listItemText}>Transaction History</Text>
												</Col>
												<View>
													<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
												</View>
											</Row>
										</Button>
									</View>
								</View>
							</Row>
						</View>
					</SafeAreaView>
				</ScrollView>
			)
		}
	}
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentWrapper:{
  	padding: 10,
  	flex: 1,
  	//backgroundColor: 'green'
  },
  letterBox:{
  	width: 46,
  	height: 46,
  	backgroundColor: '#003580',
  	borderRadius: 46,
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  details:{
  	paddingLeft: 10
  },
  firtsLetter:{
  	color: '#fff',
  	fontSize: 21,
  	fontWeight: 'bold'
  },
  name:{
  	fontSize: 18,
  	fontWeight: 'bold',
  	color: '#003580'
  },
  email:{
  	fontSize: 10,
  	color:'#A5A5A5'
  },
  subscriptionDetails:{
  	fontSize: 10
  },
  offSign:{
  	height: 30,
  	width: 30,
  	backgroundColor: '#006CFF',
  	justifyContent: 'center',
  	alignItems: 'center',
  	borderRadius: 30,
  },
  icon:{
  	color: '#fff',
  	fontSize: 20
  },
  list:{
  	paddingTop: 15
  },
  listItem:{
  	paddingHorizontal: 15,
  	paddingVertical: 0,
  	height: 30,
  	backgroundColor: '#fff',
  	borderRadius: 15,
  	shadowColor: "#000",
	shadowOffset: {
	 width: 0,
	 height: 1,
	},
	shadowOpacity: 0.22,
	shadowRadius: 2.22,
	elevation: 3
  },
  listItemContent:{
  	alignItems: 'center'
  },
  listItemText:{
  	fontSize: 14,
  	color: '#A5A5A5'
  },
  listItemIcon:{
  	fontSize: 18,
  	color: '#A5A5A5'
  }

});
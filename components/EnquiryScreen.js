import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions,TouchableOpacity, StatusBar, TextInput, Alert} from 'react-native';
import {Row, Col, Header, Container, Content, Button, Footer, FooterTab, Item, Input, Textarea, Form} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {faHome, faUser, faSearch, faEnvelope, faArrowLeft } from '@fortawesome/free-solid-svg-icons'

import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo-ads-admob';

export default class EnquiryScreen extends Component{
	constructor(props) {
    	super(props);
  	}

	_handleAccount = () =>{
		if(tokenx == ''){
			this.props.navigation.navigate('SignIn');
		}else{
			this.props.navigation.navigate('Account');
		}
	}

	_sendMessage = () =>{
		//console.log(sendName);
		fetch('http://api.hotelwaze.com/index/inquire', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				apikey: '1',
				name: this._name._lastNativeText,
				email: this._email._lastNativeText,
				hotelid: hotelIDx,
				message: this._message._lastNativeText,
			}),
		})
		.then((response) => response.json())
		.then((responseJson) => {
			if(responseJson.status == 200){
				Alert.alert(
					'Message has been sent!',
					'Your message has been sent. We will respont to you as soon as we could.',
					[
					    {text: 'OK', onPress: () => this.props.navigation.navigate('Home')},
					],
					{cancelable: false},
				)
			}else{
				alert(responseJson.msg);
			}
		})
		.catch((error) => {
			console.error(error);
		})
	}

	checkRedirect = () =>{
		if(screenNamex == 'homeScreenx'){
			this.props.navigation.navigate('Home');
		}else if(screenNamex == 'searchScreenx'){
			this.props.navigation.navigate('Search');
		}else if(screenNamex == 'singleSearchScreenx'){
			this.props.navigation.navigate('SingleSearchItem');
		}
	}


	render(){
		return(
			<Container style={styles.container}>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}} onPress={() => this.checkRedirect()}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>{hotelNamex.replace('\r\n', '')}</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content>
					{/**Google Adds**/}
					<View style={{width: '100%', display:'flex', alignItems:'center', justifyContent:'center'}}>
						<AdMobBanner
						  	bannerSize="banner"
						  	adUnitID="ca-app-pub-8722731472004532/8850355131" // Test ID, Replace with your-admob-unit-id
						  	testDeviceID="EMULATOR"
						  	servePersonalizedAds // true or false
						  	onDidFailToReceiveAdWithError={this.bannerError} 
						/>
					</View>
					<View style={styles.contentWrapper}>
						<Form style={{flex: 1}}>
							<View style={{flex: 1}}>
								<Text style={{fontSize: 10, paddingBottom: 3, color: '#a5a5a5'}}>Name<Text style={{color: 'red'}}> *</Text></Text>
								<Item rounded style={{height:30, backgroundColor:'#f1f1f1'}}>
									<TextInput
										style={{paddingHorizontal: 10, height: 30, borderColor: '#f1f1f1', fontSize: 12, width: '100%'}}
										returnKeyType="next"
										onSubmitEditing={()=>this._email.focus()}
										autoCapitalize="none"
										autoCorrect={false}
										ref={_name => this._name = _name} 
					                />
								</Item>
							</View>
							<View style={{flex: 1, paddingTop: 20}}>
								<Text style={{fontSize: 10, paddingBottom: 3, color: '#a5a5a5'}}>Email Address<Text style={{color: 'red'}}> *</Text></Text>
								<Item rounded style={{height:30, backgroundColor:'#f1f1f1'}}>
									<TextInput
										style={{paddingHorizontal: 10, height: 30, borderColor: '#f1f1f1', fontSize: 12, width: '100%'}}
										returnKeyType="next"
										onSubmitEditing={()=>this.message.focus()}
                  						keyboardType="email-address"
                  						autoCapitalize="none"
										ref={_email => this._email = _email}
					                />
								</Item>
							</View>
							<View style={{flex: 1, paddingTop: 10}}>
								<Text style={{fontSize: 10, paddingBottom: 3, color: '#a5a5a5'}}>Message<Text style={{color: 'red'}}> *</Text></Text>
								<TextInput
										style={{padding: 10,borderWidth: 0.5, backgroundColor: '#f1f1f1', borderRadius: 5, borderColor: '#f1f1f1', textAlignVertical: 'top', height: 80, width: '100%', fontSize: 12}}
										multiline={true}
    									numberOfLines={5}
					                    returnKeyType="go"
						                ref={_message => this._message = _message} 
					                />
							</View>
							<View style={{flex: 1, paddingTop: 10, alignItems: 'flex-end'}}>
								<TouchableOpacity 
									style={{width: 150, height: 30, borderRadius: 15, backgroundColor:'#006CFF', alignItems: 'center', justifyContent:'center'}} 
									onPress={()=>this._sendMessage()}
								>
									<Text style={{fontSize: 14, fontWeight:'bold', color: '#fff'}}>SEND ENQUIRY</Text>
								</TouchableOpacity>
							</View>
						</Form>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header:{
	alignItems: 'center',
	backgroundColor: '#fff',
	elevation: 0,
  },
  boxShadow:{
  	padding: 10,
  	borderRadius: 5,
	backgroundColor: '#fff',
	shadowColor: "#000",
	shadowOffset: {
		width: 0,
		height: 1,
	},
	shadowOpacity: 0.22,
	shadowRadius: 2.22,
	elevation: 3,
  },
  contentWrapper:{
  	padding: 10,
  	flex: 1,
  	//backgroundColor: 'green'
  },
  footerContainer:{
		backgroundColor: '#fff',
	},
	navActiveIcon:{
		fontSize: 20,
		color: '#000066',
	},
	navActiveText:{
		fontSize: 14,
		color: '#000066',
	},
	navInactiveIcon:{
		fontSize: 20,
		color: '#a5a5a5',
	},
	navInactiveText:{
		fontSize: 14,
		color: '#a5a5a5',
	}
});
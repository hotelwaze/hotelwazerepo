import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TouchableOpacity, Image, ImageBackground, Animated, Dimensions, Platform, NetInfo, Alert} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon, Row, Col} from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome, faUser, faSearch, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import Carousel from 'react-native-looped-carousel';

const { width, height } = Dimensions.get('window');

export default class HomeScreen extends Component{
	constructor() {
	    super();
	    this.state = {
	      size: { width, height},
	    };
	    global.activateCity = '';
	};

	componentWillMount(){
		this.CheckConnectivity();
	}

	fetchContent = () =>{
		fetch('http://api.hotelwaze.com/index/getmycountryinfo', {
	        method: 'POST',
	        headers: {
	            Accept: 'application/json',
	        },
	        body: JSON.stringify({
	            apikey: '1'
	        }),
	    })
	    .then((response) => response.json())
	    .then((responseJson) => {
	        global.myCountryNamex = responseJson.data.countryName;
	        global.countryx = responseJson.data.countryId;
	    })
	    .catch((error) => {
	        console.error(error);

	    })
	}

	_handleAccount = () =>{
		if(tokenx == ''){
			this.props.navigation.navigate('SignIn')
		}else{
			this.props.navigation.navigate('Account');
		}
	}

	_onLayoutDidChange = e => {
	    const layout = e.nativeEvent.layout;
	    this.setState({ size: { width: layout.width, height: layout.height } });
	};

	CheckConnectivity = () => {
		// For Android devices
		if (Platform.OS === "android") {
			NetInfo.isConnected.fetch().then(isConnected => {
				if (isConnected) {
					this.fetchContent();
				} else {					
				    this.props.navigation.navigate('NoConnection');
				}
			});
		} else {
			// For iOS devices
			NetInfo.isConnected.addEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
			);
		}
	};

	handleFirstConnectivityChange = isConnected => {
		NetInfo.isConnected.removeEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
		);

		if (isConnected === false) {
			this.props.navigation.navigate('NoConnection');
		} else {
			this.fetchContent();
		}
	};

	render(){
		return(
			<Container>
				<StatusBar hidden />
				<Content>
					{/*<ImageBackground source={require('../assets/wazeBg.png')} style={{width: '100%', height: 253}}>
						<View>
							<Button transparent style={{width: 50}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}>
								<Icon name='menu' style={{color: '#fff'}}></Icon>
							</Button>
						</View>
						<View style={{paddingTop: 70}}>
							<View style={{padding: 10}}>
								<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Welcome</Text>
								<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Search your favorite hotel now!</Text>
							</View>
						</View>
					</ImageBackground>*/}
					<View style={{height: 253}}>
						<Carousel
				          delay={6000}
				          style={{width: '100%', height: 253}}
				          autoplay
				          bullets={true}
				          bulletsContainerStyle={{paddingBottom: 10}}
				          currentPage={0}
				          onAnimateNextPage={p => p}>
				          <View style={[{ backgroundColor: '#BADA55' }, this.state.size]}>
				            <ImageBackground source={require('../assets/wazeBg.png')} style={{width: '100%', height: 253}}>
								{/*<View>
									<Button transparent style={{width: 50}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}>
										<Icon name='menu' style={{color: '#fff'}}></Icon>
									</Button>
								</View>*/}
								<View style={{paddingTop: 70}}>
									<View style={{padding: 10}}>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Welcome</Text>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Search your favorite hotel now!</Text>
									</View>
								</View>
							</ImageBackground>
				          </View>
				          <View style={[{ backgroundColor: 'red' }, this.state.size]}>
				            <ImageBackground source={require('../assets/wazeBg2.png')} style={{width: '100%', height: 253}}>
								{/*<View style={{paddingTop: 70}}>
									<View style={{padding: 10}}>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Welcome</Text>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Search your favorite hotel now!</Text>
									</View>
								</View>*/}
							</ImageBackground>
				          </View>
				          <View style={[{ backgroundColor: 'blue' }, this.state.size]}>
				            <ImageBackground source={require('../assets/wazeBg3.png')} style={{width: '100%', height: 253}}>
								{/*<View style={{paddingTop: 70}}>
									<View style={{padding: 10}}>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Welcome</Text>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Search your favorite hotel now!</Text>
									</View>
								</View>*/}
							</ImageBackground>
				          </View>
				          <View style={[{ backgroundColor: 'blue' }, this.state.size]}>
				            <ImageBackground source={require('../assets/wazeBg4.png')} style={{width: '100%', height: 253}}>
								{/*<View style={{paddingTop: 70}}>
									<View style={{padding: 10}}>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Welcome</Text>
										<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Search your favorite hotel now!</Text>
									</View>
								</View>*/}
							</ImageBackground>
				          </View>
				        </Carousel>
					</View>
					<View style={styles.container}>
						<TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Search')}>
							<Row style={styles.searchButton}>
								<Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
								<Text style={{paddingLeft: 5, color: '#707070'}}>Search for nearby hotel</Text>
							</Row>
						</TouchableOpacity>
					</View>
					<View style={styles.container}>
						<Text style={styles.title}>Top 7 main reasons to book directly with hotels.</Text>
					</View>
					<View style={styles.textContainer}>
						<Text style={styles.textTitle}>1. Get Better Rates</Text>
						<Text style={styles.text}>Your hotel may offer discounts and special rates if you book directly with them. Just ask them for offers!</Text>
						<Text style={styles.textTitle}>2. Cancel Anytime</Text>
						<Text style={styles.text}>Some hotels will let you cancel your booking without charging a no-show or a cancellation fee, unlike third party booking sites and services. But don’t forget to ask the hotel about their cancellation policies.</Text>
						<Text style={styles.textTitle}>3. Enjoy Special Occasions</Text>
						<Text style={styles.text}>Most hotels will do their best to accommodate your special occasion if you book directly with them. Make your stay even more special!</Text>
						<Text style={styles.textTitle}>4. Change Things Around</Text>
						<Text style={styles.text}>Most hotels will make amendments to your booking without any difficulties, as long as what you want is available.</Text>
						<Text style={styles.textTitle}>5. Get Rewards</Text>
						<Text style={styles.text}>Ask your hotel about reward points, which are often available to patrons when they book directly with the hotel. Reward programs often include discounts and freebies.</Text>
						<Text style={styles.textTitle}>6. Get Discounts on an Extra Room</Text>
						<Text style={styles.text}>Most hotels offer discounts for booking an extra room on the same day if you call late afternoon. Just ask the front desk.</Text>
						<Text style={styles.textTitle}>7. Relocate With Ease</Text>
						<Text style={styles.text}>It’s easier to ask a hotel if you want to be relocated to another hotel or to another room if you use direct booking.</Text>
					</View>
				</Content>
				<Footer>
					<FooterTab style={styles.footerContainer}>
						<Button  onPress={() => this.props.navigation.push('Home')}>
							<FontAwesomeIcon style={styles.navActiveIcon} icon={faHome} />
							<Text style={styles.navActiveText} >Home</Text>
						</Button>
						<Button onPress={() => this.props.navigation.push('Search')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faSearch} />
							<Text style={styles.navInactiveText} >Search</Text>
						</Button>
						{/*<Button onPress={() => this._handleAccount()}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faUser} />
							<Text style={styles.navInactiveText} >Account</Text>
						</Button>*/}
						<Button onPress={() => this.props.navigation.push('ContactUs')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faEnvelope} />
							<Text style={styles.navInactiveText} >Contact Us</Text>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	view:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'white'
	},
	text:{
		fontSize: 26,
		color: 'purple'
	},
	touchableHighlight:{
		width: 50,
		height: 50,
		backgroundColor: 'red'
	},
	container:{
      paddingHorizontal: 10,
      height: 45,
   },
   textContainer:{
      paddingTop: 10,
      paddingHorizontal: 10,
   },
   title:{
      fontSize: 18,
      fontWeight: 'bold',
      color: '#000066'
   },
   textTitle:{
      fontSize: 12,
      fontWeight: 'bold',
      color: '#000066'
   },
   text:{
      paddingTop: 5,
      paddingBottom: 5,
      fontSize: 10,
      color: '#000066'
   },
   searchButton:{
      height: 45,
      top: -20,
      alignItems:'center',
      paddingHorizontal: 10,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
   },
   searchIcon: {
      //padding: 10,
      marginLeft: 0,
      color: '#707070',
   },
   input: {
      flex: 1,
      paddingTop: 10,
      paddingRight: 10,
      paddingBottom: 10,
      paddingLeft: 0,
      backgroundColor: '#fff',
      color: '#707070',
      borderRadius: 10,
      fontSize: 20
   },
   footerContainer:{
		backgroundColor: '#fff',
	},
	navActiveIcon:{
		fontSize: 20,
		color: '#000066',
	},
	navActiveText:{
		fontSize: 14,
		color: '#000066',
	},
	navInactiveIcon:{
		fontSize: 20,
		color: '#a5a5a5',
	},
	navInactiveText:{
		fontSize: 14,
		color: '#a5a5a5',
	}
});
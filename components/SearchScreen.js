import React, { Component } from 'react';
import {
    NetInfo,
    Animated, 
    Platform, 
    StatusBar,
    StyleSheet, 
    Text, 
    View, 
    RefreshControl, 
    TouchableOpacity, 
    Linking, 
    ImageBackground, 
    TextInput, 
    ScrollView, 
    FlatList,
    TouchableHighlight
} from 'react-native';

import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon, Row, Col} from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMapMarkerAlt, faPhone, faGlobe, faHome, faUser, faSearch, faEnvelope, faVolumeMute, faVolumeUp } from '@fortawesome/free-solid-svg-icons';

import { Audio } from 'expo-av';

import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo-ads-admob';

const HEADER_MAX_HEIGHT = 210;
const HEADER_MIN_HEIGHT = 65;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

global.latitudex = '';
global.longitudex = '';
global.hotelNamex = '';
global.hotelPhonex = '';
global.hotelAddress1x = '';
global.hotelWebsitex = '';
global.activateCity = '';
global.acivate = '';
global.statex = '';
global.cityx = '';
global.audiox = 1;

const audioSound = {
    one: require('../audio/officialwebsite.mp3'),
}

export default class SearchScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            soundControl: true,
            isLoading: true,
            data: [],
            current_page: 0,
            loadingMore: false,
            loading: true,
            error: null,
            refreshing:false,
            hasMore: true,
            searchKeyword:'',
            scrollY: new Animated.Value(
            // iOS has negative initial scroll value because content inset...
            Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
        ),
        refreshing: false,
        };
        
    };


    toggleSound = () =>{
        if(audiox == 1){
            global.audiox = 0;
            this.setState({soundControl: false});
        }else{
            global.audiox = 1;
            this.setState({soundControl: true});
        }
    }     

    handlePlay = async (_play, website) => {
        if(audiox == 1){
            const soundObject = new Audio.Sound();
            try {
                let sourcex  = audioSound[_play]
                await soundObject.loadAsync(sourcex);
                await soundObject
                    .playAsync()
                    .then(async playbackStatus => {
                        setTimeout(() => {
                            Linking.openURL(website);
                        }, 3000)
                       
                    })
                    .catch(error => {
                        console.log(error)
                    })
            } catch (error) {
                console.log(error)
            }
        }else{
            Linking.openURL(website);
        }
    }



    componentWillMount(){
        this.CheckConnectivity();
    }

    CheckConnectivity = () => {
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                } else {                    
                    this.props.navigation.navigate('NoConnection');
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
            );
        }
    };

    handleFirstConnectivityChange = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChange
        );

        if (isConnected === false) {
            this.props.navigation.navigate('NoConnection');
        } else {

        }
    };

    _handleLoadMore= () =>{

        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this._handleLoadMoreFetch();
                                console.log('connected');
                } else {                    
                    this.props.navigation.navigate('NoConnection');
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
            "connectionChange",
            this.handleFirstConnectivityChangeB
            );
        }
    }

    handleFirstConnectivityChangeB = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChangeB
        );

        if (isConnected === false) {
            this.props.navigation.navigate('NoConnection');
        } else {
            this._handleLoadMoreFetch();
        }
    };

    _handleLoadMoreFetch = () =>{
        let currentPage = parseInt(this.state.current_page)+ 1;
        this.setState({ 
            loading: true,
            current_page: currentPage
        });

        let searchKeyword = this.state.searchKeyword;
        fetch('http://api.hotelwaze.com/index/searchHotel', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                token: tokenx,
                keywords: searchKeyword,
                country_id: countryx,
                state_id: statex,
                city_id: cityx,
                page: currentPage
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                data: [...this.state.data, ...responseJson.data],
                loadingMore: false
            })
        })
        .catch((error) => {
            this.setState({ loading: false })
            console.error(error);
        })
    }



   _handleSearch = () =>{
       this.ListView_Ref.scrollToOffset({ offset: 0,  animated: true });
        // For Android devices
        if (Platform.OS === "android") {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {
                    this.handleSearchFetch();
                } else {                    
                    this.props.navigation.navigate('NoConnection');
                }
            });
        } else {
            // For iOS devices
            NetInfo.isConnected.addEventListener(
            "connectionChange",
            this.handleFirstConnectivityChangeA
            );
        }
    };

    handleFirstConnectivityChangeA = isConnected => {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.handleFirstConnectivityChangeA
        );

        if (isConnected === false) {
            this.props.navigation.navigate('NoConnection');
        } else {
            this.handleSearchFetch();
        }
    };

    handleSearchFetch = () =>{
        this.setState({ 
            loading: true,
            current_page: 0
        });
        fetch('http://api.hotelwaze.com/index/searchHotel', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                token: tokenx,
                keywords: keywordx,
                country_id: countryx,
                state_id: statex,
                city_id: cityx,
                page: 0
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                data:responseJson.data,
            })
        })
        .catch((error) => {
            console.error(error);
        })
    }



    _handleAccount = () =>{
        if(tokenx == ''){
            this.props.navigation.navigate('SignIn')
        }else{
            this.props.navigation.navigate('Account');
        }
    };

    passCordinates = (latitude, longitude, name, phone, address1, website, hotelID, permission) =>{
        global.latitudex = parseFloat(latitude);
        global.longitudex = parseFloat(longitude);
        global.hotelNamex = name;
        global.hotelPhonex = phone;
        global.hotelAddress1x = address1;
        global.hotelWebsitex = website;
        global.hotelIDx = hotelID;
        global.permissionx = permission;
        this.props.navigation.navigate('SingleSearchItem');

    };

    componentDidMount(){
        fetch('http://api.hotelwaze.com/index/searchtopten', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                countryId:countryx
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
              data: [...this.state.data, ...responseJson.data],
              loadingMore: true,
            })
        })

        .catch((error) => {
            console.error(error);
            this.setState({ loading: false })
        })
    }

    updateCountry = () =>{
        global.acivate = '1';
        this.props.navigation.navigate('SelectCountry');
    }

    _renderFooter = () => {
        if(this.state.data.length != 0){
           return <View style={{flex: 1}}><Text style={{textAlign: 'center'}}>Loading Hotels...</Text></View>;
       }else{
           return <View style={{flex: 1}}><Text style={{textAlign: 'center'}}>No Hotels Found!</Text></View>;
       } 
    };

    renderToggle(){
        if(audiox == 1){
           return <FontAwesomeIcon style={styles.navActiveIcon} icon={faVolumeUp}/>
        }else{
            return <FontAwesomeIcon style={styles.navInactiveIcon} icon={faVolumeMute}/>
        }
    }

    passScreenName = (name, screenName, hotelID) =>{
        global.hotelNamex = name;
        global.screenNamex = screenName;
        global.hotelIDx = hotelID;
        this.props.navigation.navigate('Enquiry');
    }


    render() {
        const { navigation } = this.props;

        global.keywordx = navigation.getParam('keywordName', '');
        global.countryx = navigation.getParam('countryId', countryx);
        global.statex = navigation.getParam('stateId', '');
        global.cityx = navigation.getParam('cityId', '');

        if(acivate == '1'){
            global.stateNamex = 'Select State';
            global.cityNamex = 'Select City';
            global.statex = '';
            global.cityx = '';
        }else{
            global.stateNamex = navigation.getParam('stateName', 'Select State');
            if(activateCity == '1'){
                global.cityNamex = 'Select City';
                global.cityx = '';
            }else{
                global.cityNamex = navigation.getParam('cityName', 'Select City');
            }
        }

        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );

        const headMov = scrollY.interpolate({
          inputRange: [0, 180, 181],
          outputRange: [0, -35, -35]
        });

        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });

        const imageOpacityx = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, 1],
            extrapolate: 'clamp',
        });

        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const searchPosition = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });


        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });

        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -8],
            extrapolate: 'clamp',
        });

        
        return (
            <View style={styles.fill}>
                <StatusBar hidden/>
                {/**Google Adds**/}
                <View style={{width: '100%', display:'flex', alignItems:'center', justifyContent:'center'}}>
                    <AdMobBanner
                        bannerSize="banner"
                        adUnitID="ca-app-pub-8722731472004532/4655318625" // Test ID, Replace with your-admob-unit-id
                        testDeviceID="EMULATOR"
                        servePersonalizedAds // true or false
                        onDidFailToReceiveAdWithError={this.bannerError} 
                    />
                </View>
                <FlatList
                    data={this.state.data}
                    extraData={this.state}
                    ref={(ref) => {
                        this.ListView_Ref = ref;
                    }}  
                    renderItem={
                        ({item, index}) =>
                        <View style={{paddingTop: 0, backgroundColor: '#fff'}}>
                            <View>
                                <View key={item.hotel_id} style={styles.searchResultContainer}>
                                    <TouchableHighlight onPress={()=> 
                                        this.passCordinates(
                                            item.latitude, 
                                            item.longitude, 
                                            item.name, 
                                            item.telephone_number,
                                            item.address_1,
                                            item.website,
                                            item.hotel_id,
                                            item.msg_permission
                                        )
                                    }>
                                        <ImageBackground imageStyle={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }} 
                                            source = {(item.logo_path != '') ? {uri: item.logo_path} : {uri: 'https://www.hotelwaze.com/assets/img/noavailable.jpg'}} 
                                            style={{width: '100%', height: 126}}>
                                        </ImageBackground>
                                    </TouchableHighlight>
                                    <View style={styles.searchResultInfo}>
                                        <TouchableHighlight onPress={()=> 
                                            this.passCordinates(
                                                item.latitude, 
                                                item.longitude, 
                                                item.name, 
                                                item.telephone_number,
                                                item.address_1,
                                                item.website,
                                                item.hotel_id,
                                                item.msg_permission
                                            )
                                        }>
                                            <Text style={styles.searchResultTitle}>{item.name.replace('\r\n', '')}</Text>
                                        </TouchableHighlight>
                                        <TouchableHighlight onPress={()=> 
                                            this.passCordinates(
                                                item.latitude, 
                                                item.longitude, 
                                                item.name, 
                                                item.telephone_number,
                                                item.address_1,
                                                item.website,
                                                item.hotel_id,
                                                item.msg_permission
                                            )
                                        }>
                                            <Row style={styles.searcResultTitleContainer}>
                                                <FontAwesomeIcon style={styles.searhResultInfoIcon} icon={faMapMarkerAlt} />
                                                <Text style={styles.searchResultInfoTitle}>{item.address_1.replace('\r\n', '')}</Text>
                                            </Row>
                                        </TouchableHighlight>
                                        <Row style={{paddingTop: 15, paddingBottom: 5}}>
                                            <Col style={{paddingRight: 15}}>
                                                <TouchableOpacity style={styles.contactInfo} onPress={()=>{Linking.openURL(`tel:${item.telephone_number}`)}}>
                                                    <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                                        <View style={{padding: 15}}>
                                                            <View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
                                                                <View style={{paddingBottom: 5}}>
                                                                    <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faPhone} />
                                                                </View>
                                                                <Text style={styles.searchResultInfoTitlex}>Contact</Text>
                                                                <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            </Col>
                                            <Col style={{paddingRight: 15}}>
                                                {item.msg_permission == 1 ?
                                                    <TouchableOpacity style={styles.contactInfo}
                                                        onPress={() =>
                                                            this.passScreenName(
                                                                item.name,
                                                                'searchScreenx',
                                                                item.hotel_id
                                                            )
                                                    }>
                                                        <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                                            <View style={{padding: 15}}>
                                                                <View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
                                                                    <View style={{paddingBottom: 5}}>
                                                                        <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faEnvelope} />
                                                                    </View>
                                                                    <Text style={styles.searchResultInfoTitlex}>Enquire</Text>
                                                                    <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                   
                                                    : 
                                                    
                                                    <TouchableOpacity style={styles.contactInfo} disabled={true}>
                                                        <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                                            <View style={{padding: 15}}>
                                                                <View style={{display:'flex', alignItems:'center',justifyContent:'center',backgroundColor: '#fff', opacity:0.4}}>
                                                                    <View style={{paddingBottom: 5}}>
                                                                        <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faEnvelope} />
                                                                    </View>
                                                                    <Text style={styles.searchResultInfoTitlex}>Enquire</Text>
                                                                    <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                }
                                            </Col>
                                            <Col style={{paddingRight: 15}}>
                                                <TouchableOpacity style={styles.contactInfo} onPress={()=>{this.handlePlay('one',item.website)}}>
                                                    <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
                                                        <View style={{padding: 15}}>
                                                            <View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
                                                                <View style={{paddingBottom: 5}}>
                                                                    <FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faGlobe} />
                                                                </View>
                                                                <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
                                                                <Text style={styles.searchResultInfoTitlex}>Website</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            </Col>
                                            <Col>
                                                
                                            </Col>
                                        </Row>
                                    </View>
                                </View>
                            </View>
                        </View>
                    }
                    keyExtractor={item => item.hotel_id}
                    onEndReached={this._handleLoadMore}
                    onEndReachedThreshold={5}
                    initialNumToRender={10}
                    refreshing={this.state.refreshing}
                    //ListEmptyComponent={this.ListEmptyView}
                    ListFooterComponent = {this._renderFooter}
                />
                <Animated.View
                    pointerEvents="none"
                    style={[
                        styles.header,
                        { 
                            transform: [{ 
                                translateY: headerTranslate 
                            }] 
                        },
                    ]}
                >

                </Animated.View>
                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                { 
                                    translateY: headerTranslate 
                                },
                            ],
                        },
                    ]}
                >
                    <Animated.View 
                        style={[
                            styles.title,
                            {
                                opacity: imageOpacity
                            }
                        ]}
                    >
                        <View style={styles.searchContent}>
                            <Row style={{height: 20}}>
                                <Col>
                                    <Text style={{fontSize: 10, paddingBottom: 5}}>Keyword</Text>
                                </Col>
                                <Col>
                                    <View style={{alignItems: 'flex-end'}}>
                                        <TouchableOpacity 
                                            onPress={()=>this.toggleSound()} 
                                            style={this.state.soundControl ? styles.toggleButton : styles.toggleButtonOff}>
                                                {this.state.soundControl ? <FontAwesomeIcon style={styles.navActiveIcon, {color: '#fff'}} icon={faVolumeUp}/> : <FontAwesomeIcon style={styles.navInactiveIcon, {color: '#fff'}} icon={faVolumeMute}/>}
                                        </TouchableOpacity>
                                    </View>
                                </Col>
                            </Row>
                            <View styles={styles.searchInput}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('SelectKeyword')}>
                                    <View style={{backgroundColor: '#f1f1f1', height: 30, borderRadius: 15, justifyContent: 'center', paddingHorizontal: 5}}>
                                        <Text style={styles.searchFilterTitleMain}>{navigation.getParam('keywordName', 'Search Keyword')}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1, paddingTop:20}}>
                                <Row style={styles.searchInfo}>
                                    <Col>
                                        <TouchableOpacity onPress={() => this.updateCountry()}>
                                            <View style={{borderRightWidth: 0.5, borderRightColor: '#f1f1f1', marginRight: 15}}>
                                                <Text style={{fontSize: 10}}>Country</Text>
                                                <Text style={styles.searchFilterTitle}>{navigation.getParam('countryName', myCountryNamex)}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Col>
                                    <Col>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SelectState')}>
                                            <View style={{borderRightWidth: 0.5, borderRightColor: '#f1f1f1', marginRight: 15}}>
                                                <Text style={{fontSize: 10}}>State</Text>
                                                <Text style={styles.searchFilterTitle}>{stateNamex}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Col>
                                        <Col>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SelectCity')}>
                                            <View style={{marginRight: 15}}>
                                                <Text style={{fontSize: 10}}>City</Text>
                                                <Text style={styles.searchFilterTitle}>{cityNamex}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                            </View>
                            <View style={{flex: 1, paddingTop: 10}}>
                                <TouchableOpacity style={styles.buttonSearch} onPress={()=>this._handleSearch()}>
                                    <Text style={styles.buttonText}>SEARCH</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Animated.View>
                    <Animated.View
                        style={[
                            styles.searchButton,
                            {
                                opacity: imageOpacityx,
                                translateY: headMov
                            }
                        ]}
                    >
                        <View style={{flex: 1, paddingHorizontal: 10, width: '100%'}}>
                            <TouchableOpacity style={{flex: 1, zIndex: 9999, width: '100%'}}>
                                <Row style={{flex: 1, width: '100%', alignItems: 'center'}}>
                                    <Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
                                    <Text style={{paddingLeft: 5, color: '#707070'}}>Search for nearby hotel</Text>
                                </Row>
                            </TouchableOpacity>
                        </View>
                    </Animated.View>
                </Animated.View>
                <Footer>
                    <FooterTab style={styles.footerContainer}>
                        <Button  onPress={() => this.props.navigation.push('Home')}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faHome} />
                            <Text style={styles.navInactiveText} >Home</Text>
                        </Button>
                        <Button onPress={() => this.props.navigation.push('Search')}>
                            <FontAwesomeIcon style={styles.navActiveIcon} icon={faSearch} />
                            <Text style={styles.navActiveText} >Search</Text>
                        </Button>
                        <Button onPress={() => this.props.navigation.push('ContactUs')}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faEnvelope} />
                            <Text style={styles.navInactiveText} >Contact Us</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    toggleButton:{
        backgroundColor:'#006CFF',
        width: 25, 
        top: -10, 
        height: 25, 
        padding:5, 
        display:'flex', 
        alignItems: 'center', 
        justifyContent:'center', 
        borderRadius: 25
    },
    toggleButtonOff:{
        backgroundColor:'#818BA7',
        width: 25, 
        top: -10, 
        height: 25, 
        padding:5, 
        display:'flex', 
        alignItems: 'center', 
        justifyContent:'center', 
        borderRadius: 25
    },
    fill: {
        flex: 1,
        paddingTop: HEADER_MAX_HEIGHT
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#006CFF',
        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    bar:{
        backgroundColor: 'transparent',
        //marginTop: Platform.OS === 'ios' ? 28 : 38,
        //height: 32,
        //alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: HEADER_MAX_HEIGHT
    },
    title: {
        //justifyContent: 'center',
        height: HEADER_MAX_HEIGHT,
        backgroundColor: '#fff',
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        //paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
        paddingTop: HEADER_MAX_HEIGHT,
    },
    container:{
        paddingHorizontal: 10,
    },
    searchContent:{
        flex: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 20,
        margin: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    contactInfo:{
        display:'flex', 
        alignItems:'center', 
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height:2,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    searchResultInfoTitlex:{
        textAlign: 'center',
        color: '#003580',
        fontSize: 8,
        paddingLeft: 5,
    },
    searhResultInfoIconx:{
        color: '#003580',
        fontSize: 31,
    },
    searchInput:{
        backgroundColor: '#f1f1f1',
        position: 'absolute', 
        top: 0,
        left: 0,
        right: 0,
    },
    input:{
        height: 30,
        padding: 10,
        fontSize: 10,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
    },
    searchInfo:{
        borderTopWidth: 1,
        borderTopColor: '#f1f1f1',
        borderBottomWidth: 1,
        borderBottomColor: '#f1f1f1',
        paddingVertical: 5,
    },
    searchFilterTitleMain:{
        color: '#c0c0c0',
        fontSize: 10
    },
    searchFilterTitle:{
        color: '#000',
        fontSize: 10,
        fontWeight: 'bold',
    },
    buttonSearch:{
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:5,
        height: 30,
        backgroundColor: '#006CFF', 
    },
    buttonText:{
        color: '#fff',
        fontSize: 10,
        fontWeight: 'bold',
    },
    titlex:{
        fontSize: 10,
        color: '#a5a5a5',
        paddingBottom: 5,
    },
   searchResultContainer:{
        flex: 1, 
        marginVertical: 10,
        marginHorizontal: 20,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 5,
    },
    searchResultInfo:{
        padding: 10,
    },
    searchResultTitle:{
        color: '#003580',
        fontSize: 18,
        fontWeight: 'bold',
    },
    searcResultTitleContainer:{
        paddingTop: 5,
    },
    searhResultInfoIcon:{
        color: '#003580',
        fontSize: 10,
    },
    searchResultInfoTitle:{
        color: '#003580',
        fontSize: 12,
        paddingLeft: 5,
    },
   footerContainer:{
        backgroundColor: '#fff',
    },
    navActiveIcon:{
        fontSize: 20,
        color: '#000066',
    },
    navActiveText:{
        fontSize: 14,
        color: '#000066',
    },
    navInactiveIcon:{
        fontSize: 20,
        color: '#a5a5a5',
    },
    navInactiveText:{
        fontSize: 14,
        color: '#a5a5a5',
    },
    textContainer:{
      paddingTop: 10,
      paddingHorizontal: 10,
    },
    titlex:{
      fontSize: 18,
      fontWeight: 'bold',
   },
   textTitle:{
      fontSize: 12,
      fontWeight: 'bold',
   },
   text:{
      paddingTop: 5,
      paddingBottom: 5,
      fontSize: 10
   },
   searchButton:{
        marginHorizontal: 10,
        height: 45,
        top: -20,
        alignItems:'center',
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
   },
   searchIcon: {
      marginLeft: 0,
      color: '#707070',
   },
});
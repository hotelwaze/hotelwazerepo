import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity, Image, Alert, AsyncStorage} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem, Button} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

export default class AccountChangeEmail extends Component{

	_handleLogout = async() => {
		try {
		  await AsyncStorage.removeItem('_signInSession');
		  const value = await AsyncStorage.getItem('_signInSession');
		  global.tokenx = '';
		  this._out(); 
		} catch (error) {
		  console.log("Error resetting data"+error);
		}
	};

	_out = () =>{
		console.log(tokenx);
		Alert.alert(
			'Subscription Cancelled',
			'Your subscription is now cancelled',
			[
				{
					text: 'OK', onPress: () => this.props.navigation.navigate('SignIn')
				},
			],
			{
				cancelable: false
			},
		);
		
	}

	render(){
		return(
			<Container>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}}onPress={() => this.props.navigation.navigate('Account')}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>Change Email</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content style={{paddingHorizontal: 20}}>
					<View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 12}}>You are cancelling your subscription to Hotelwaze.</Text>
							<Text style={{fontSize: 12}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</Text>
						</View>
						<View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
							<Button style={{width:250, height: 30, backgroundColor: '#C70000', justifyContent: 'center', alignItems: 'center', borderRadius: 30}} onPress={this._handleLogout}>
								<Text style={{color: '#fff'}}>Cancel Subscription</Text>
							</Button>
						</View>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({	
	container:{
		padding: 10,
	},
	header:{
		alignItems: 'center',
		backgroundColor: '#fff',
		elevation: 0,
	},
	boxContainer:{
		flex: 1,
		borderRadius: 5,
		margin: 10,
		backgroundColor: '#fff',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
	},
	title:{
		fontSize: 10,
		color: '#a5a5a5',
		paddingBottom: 5,
	},
	searchInput:{
		backgroundColor: '#f1f1f1',
		position: 'absolute', 
		top: 0,
		left: 0,
		right: 0,
	},
	input:{
		height: 30,
        padding: 10,
        fontSize: 10,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
	},
	list:{
		paddingTop: 15
	},
	listItem:{
		paddingHorizontal: 15,
		paddingVertical: 0,
		height: 30,
		backgroundColor: '#fff',
		borderRadius: 15,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3
	},
	listItemContent:{
		alignItems: 'center'
	},
	listItemText:{
		fontSize: 14,
		color: '#A5A5A5'
	},
	listItemIcon:{
		fontSize: 18,
		color: '#A5A5A5'
	},
	
});
import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TouchableOpacity, Image, ImageBackground, Animated, Dimensions, Platform, NetInfo, Alert, ScrollView} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon, Row, Col} from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome, faUser, faSearch, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import Carousel from 'react-native-banner-carousel';
import HTML from 'react-native-render-html';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;

export default class HomeScreen extends Component{
	constructor() {
	    super();
	    this.state = {
	      //size: { width, height},
	      counter: 0,
	      data:[],
	      htmlContent: [],
	    };
	    global.activateCity = '';
	    this.fetchHTML();
	};

	componentWillMount(){
		this.CheckConnectivity();
	};

	fetchContent = () =>{
		fetch('http://api.hotelwaze.com/index/getmycountryinfo', {
	        method: 'POST',
	        headers: {
	            Accept: 'application/json',
	        },
	        body: JSON.stringify({
	            apikey: '1'
	        }),
	    })
	    .then((response) => response.json())
	    .then((responseJson) => {
	    	this.fetchBanner();
	        global.myCountryNamex = responseJson.data.countryName;
	        global.countryx = responseJson.data.countryId;
	    })
	    .catch((error) => {
	        console.error(error);

	    })
	};

	fetchHTML = () =>{
		fetch('http://api.hotelwaze.com/index/apphomecontent', {
	        method: 'POST',
	        headers: {
	            Accept: 'application/json',
	        },
	        body: JSON.stringify({
	            apikey: '1'
	        }),
	    })
	    .then((response) => response.json())
	    .then((responseJson) => {
	    	this.setState({
                htmlContent:responseJson.data.content,
            });
	    })
	    .catch((error) => {
	        console.error(error);

	    })
	};

	fetchBanner = () =>{
		fetch('http://api.hotelwaze.com/index/banner', {
	        method: 'POST',
	        headers: {
	            Accept: 'application/json',
	        },
	        body: JSON.stringify({
	            apikey: '1'
	        }),
	    })
	    .then((response) => response.json())
	    .then((responseJson) => {
	    	this.setState({
                data:responseJson.data,
            });
	    })
	    .catch((error) => {
	        console.error(error);

	    })
	};

	_handleAccount = () =>{
		if(tokenx == ''){
			this.props.navigation.navigate('SignIn')
		}else{
			this.props.navigation.navigate('Account');
		}
	};

	/*_onLayoutDidChange = e => {
	    const layout = e.nativeEvent.layout;
	    this.setState({ size: { width: layout.width, height: layout.height } });
	};
*/
	CheckConnectivity = () => {
		// For Android devices
		if (Platform.OS === "android") {
			NetInfo.isConnected.fetch().then(isConnected => {
				if (isConnected) {
					this.fetchContent();
				} else {					
				    this.props.navigation.navigate('NoConnection');
				}
			});
		} else {
			// For iOS devices
			NetInfo.isConnected.addEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
			);
		}
	};

	handleFirstConnectivityChange = isConnected => {
		NetInfo.isConnected.removeEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
		);

		if (isConnected === false) {
			this.props.navigation.navigate('NoConnection');
		} else {
			this.fetchContent();
		}
	};

	renderPage(image, index) {
        return (
            <View key={index}>
                <Image style={{ width: BannerWidth, height: BannerHeight }} source={{ uri: image['images'] }} />
            </View>
        );
    }

	render(){
		const imagex = this.state.data;
		const htmlx = this.state.htmlContent;
		return(
			<Container>
				<StatusBar hidden />
				<Content>
					{/*<ImageBackground source={require('../assets/wazeBg.png')} style={{width: '100%', height: 253}}>
						<View>
							<Button transparent style={{width: 50}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}>
								<Icon name='menu' style={{color: '#fff'}}></Icon>
							</Button>
						</View>
						<View style={{paddingTop: 70}}>
							<View style={{padding: 10}}>
								<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Welcome</Text>
								<Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Search your favorite hotel now!</Text>
							</View>
						</View>
					</ImageBackground>*/}
					<View style={{height: 253, flex: 1, backgroundColor:'#fff', justifyContent:'center'}}>
						<Carousel
		                    autoplay
		                    autoplayTimeout={6000}
		                    loop
		                    showsPageIndicator
		                    pageIndicatorContainerStyle={{bottom: 30}}
		                    activePageIndicatorStyle={{backgroundColor:'#fff'}}
		                    index={0}
		                    pageSize={BannerWidth}
		                >
                    		{imagex.map((image, index) => this.renderPage(image, index))}
                		</Carousel>
					</View>
					<View style={styles.container}>
						<TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Search')}>
							<Row style={styles.searchButton}>
								<Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
								<Text style={{paddingLeft: 5, color: '#707070'}}>Search for nearby hotel</Text>
							</Row>
						</TouchableOpacity>
					</View>
					<ScrollView style={{ flex: 1, paddingHorizontal: 20, marginTop: 10, paddingBottom: 20 }}>
		                <HTML
		                	html={htmlx}
		                	classesStyles={
		                		{
		                			'style-1':{
		                				color: 'green'
		                			}
		                		}
		                	}
		                	tagsStyles={ 
		                		{
		                			h1: { 
		                				fontSize: 18,
		                				color: '#000066',
		                				margin: 0,
		                				paddingBottom: 10 
		                			},
		                			h2:{
		                				fontSize: 17,
		                				color:'#000066',
		                				margin: 0,
		                				paddingBottom: 10
		                			},
		                			h3:{
		                				fontSize: 16,
		                				color:'#000066',
		                				margin: 0,
		                				paddingBottom: 10
		                			},
		                			h4:{
		                				fontSize: 14,
		                				color:'#000066',
		                				margin: 0,
		                				paddingBottom: 10
		                			},
		                			p:{
		                				fontSize: 12,
		                				color: '#000066',
		                				margin: 0,
		                				paddingBottom: 5
		                			}	
		                		}
		                	}

		                	imagesMaxWidth={Dimensions.get('window').width} 
		                />
		            </ScrollView>
				</Content>
				<Footer>
					<FooterTab style={styles.footerContainer}>
						<Button  onPress={() => this.props.navigation.push('Home')}>
							<FontAwesomeIcon style={styles.navActiveIcon} icon={faHome} />
							<Text style={styles.navActiveText} >Home</Text>
						</Button>
						<Button onPress={() => this.props.navigation.push('Search')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faSearch} />
							<Text style={styles.navInactiveText} >Search</Text>
						</Button>
						{/*<Button onPress={() => this._handleAccount()}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faUser} />
							<Text style={styles.navInactiveText} >Account</Text>
						</Button>*/}
						<Button onPress={() => this.props.navigation.push('ContactUs')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faEnvelope} />
							<Text style={styles.navInactiveText} >Contact Us</Text>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}


const styles = StyleSheet.create({
	view:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'white'
	},
	text:{
		fontSize: 26,
		color: 'purple'
	},
	touchableHighlight:{
		width: 50,
		height: 50,
		backgroundColor: 'red'
	},
	container:{
      paddingHorizontal: 10,
      height: 45,
   },
   textContainer:{
      paddingTop: 10,
      paddingHorizontal: 10,
   },
   title:{
      fontSize: 18,
      fontWeight: 'bold',
      color: '#000066'
   },
   textTitle:{
      fontSize: 12,
      fontWeight: 'bold',
      color: '#000066'
   },
   text:{
      paddingTop: 5,
      paddingBottom: 5,
      fontSize: 10,
      color: '#000066'
   },
   searchButton:{
      height: 45,
      top: -20,
      alignItems:'center',
      paddingHorizontal: 10,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
   },
   searchIcon: {
      //padding: 10,
      marginLeft: 0,
      color: '#707070',
   },
   input: {
      flex: 1,
      paddingTop: 10,
      paddingRight: 10,
      paddingBottom: 10,
      paddingLeft: 0,
      backgroundColor: '#fff',
      color: '#707070',
      borderRadius: 10,
      fontSize: 20
   },
   footerContainer:{
		backgroundColor: '#fff',
	},
	navActiveIcon:{
		fontSize: 20,
		color: '#000066',
	},
	navActiveText:{
		fontSize: 14,
		color: '#000066',
	},
	navInactiveIcon:{
		fontSize: 20,
		color: '#a5a5a5',
	},
	navInactiveText:{
		fontSize: 14,
		color: '#a5a5a5',
	}
});
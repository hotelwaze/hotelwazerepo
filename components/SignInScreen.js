import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem, Button} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';
import {AsyncStorage} from 'react-native';

export default class SignInScreen extends Component{
	_signIn = () =>{
		fetch('http://api.hotelwaze.com/index/login', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				apikey: 1,
				email: this._email._lastNativeText,
        		password: this._password._lastNativeText,
			}),
		})
		.then((response) => response.json())
		.then((responseJson) => {
			AsyncStorage.setItem('_signInSession', JSON.stringify(responseJson), () => {
				AsyncStorage.getItem('_signInSession')
				.then((result) => {
					const data = JSON.parse(result);
					global.tokenx = data.token;
					global.firstNamex = data.data.first_name;
					global.lastNamex = data.data.last_name;
					global.emailx = data.data.email_address;
					this.props.navigation.navigate('Home');
				});
			});
		})
		.catch((error) => {
			console.error(error);
		})
	}

	render(){
		return(
			<Container>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}}onPress={() => this.props.navigation.navigate('Home')}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>Sign In</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content>
					<Text style={{paddingBottom: 10, paddingHorizontal: 10}}>You can sign in using your Hotelwaze.com account to access our services.</Text>
					<View style={styles.boxContainer}>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Email Address</Text>
							<TextInput 
								style={styles.input} 
			                  	returnKeyType="next"
			                  	onSubmitEditing={()=>this.passwordInput.focus()}
			                  	keyboardType="email-address"
			                  	autoCapitalize="none"
			                  	autoCorrect={false}
			                  	ref={_email => this._email = _email} 
			                />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Password</Text>
							<TextInput style={styles.input} 
			                  	returnKeyType="go"
			                  	secureTextEntry
			                  	ref={_password => this._password = _password}
			                />
						</View>
						<View style={{alignItems: 'flex-end'}}>
							<Button style={{width: 105, height: 30, borderRadius: 15, backgroundColor: '#006CFF'}} onPress={()=>this._signIn()}>
								<Text style={{textAlign: 'center', width: '100%', color: '#fff', fontSize: 10}}>Sign In</Text>
							</Button>
						</View>
					</View>
					<Text style={{paddingTop: 10, paddingHorizontal: 10, fontSize: 14, color:'#003580', fontWeight: 'bold'}}>Don't have an account yet?</Text>
					<Row>	
						<View style={{flex: 1, paddingBottom: 15, paddingHorizontal: 10}}>
							<View style={styles.list}>
								<Button transparent style={styles.listItem} onPress={() => this.props.navigation.navigate('Register')}>
									<Row style={styles.listItemContent}>
										<Col>
											<Text style={styles.listItemText}>Sign Up</Text>
										</Col>
										<View>
											<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
										</View>
									</Row>
								</Button>
							</View>
							<View style={styles.list}>
								<Button transparent style={styles.listItem}>
									<Row style={styles.listItemContent}>
										<Col>
											<Text style={styles.listItemText}>Forgot Password</Text>
										</Col>
										<View>
											<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
										</View>
									</Row>
								</Button>
							</View>
							<Text style={{fontSize: 10, paddingTop: 10}}>By signing in or creating an account, you agree with our <Text style={{color:'#006CFF'}}>Terms & conditions</Text></Text>
						</View>
					</Row>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({	
	container:{
		padding: 10,
	},
	header:{
		alignItems: 'center',
		backgroundColor: '#fff',
		elevation: 0,
	},
	boxContainer:{
		flex: 1,
		borderRadius: 5,
		paddingHorizontal: 10,
		paddingVertical: 20,
		margin: 10,
		backgroundColor: '#fff',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
	},
	title:{
		fontSize: 10,
		color: '#a5a5a5',
		paddingBottom: 5,
	},
	searchInput:{
		backgroundColor: '#f1f1f1',
		position: 'absolute', 
		top: 0,
		left: 0,
		right: 0,
	},
	input:{
		height: 30,
        padding: 10,
        fontSize: 10,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
	},
	list:{
		paddingTop: 15
	},
	listItem:{
		paddingHorizontal: 15,
		paddingVertical: 0,
		height: 30,
		backgroundColor: '#fff',
		borderRadius: 15,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3
	},
	listItemContent:{
		alignItems: 'center'
	},
	listItemText:{
		fontSize: 14,
		color: '#A5A5A5'
	},
	listItemIcon:{
		fontSize: 18,
		color: '#A5A5A5'
	},
	
});
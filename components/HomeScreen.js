import React, {Component} from 'react';
import {
	StatusBar, 
	StyleSheet, 
	Text, 
	View, 
	TouchableOpacity, 
	Image, 
	ImageBackground, 
	Animated, 
	Dimensions, 
	Platform, 
	NetInfo, 
	Alert, 
	ScrollView,
	FlatList,
    TouchableHighlight,
    Linking
} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon, Row, Col} from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome, faUser, faSearch, faEnvelope, faMapMarkerAlt, faPhone, faGlobe} from '@fortawesome/free-solid-svg-icons';

import { Audio } from 'expo-av';

import Carousel from 'react-native-banner-carousel';

import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo-ads-admob';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;
const audioSound = {
    one: require('../audio/officialwebsite.mp3'),
}

export default class HomeScreen extends Component{
	constructor() {
	    super();
	    this.state = {
	      counter: 0,
	      data:[],
	      datax:[],
	    };
	    global.activateCity = '';
	};

	componentWillMount(){
		this.CheckConnectivity();
	};

	handlePlay = async (_play, website) => {
        if(audiox == 1){
            const soundObject = new Audio.Sound();
            try {
                let sourcex  = audioSound[_play]
                await soundObject.loadAsync(sourcex);
                await soundObject
                    .playAsync()
                    .then(async playbackStatus => {
                        setTimeout(() => {
                            Linking.openURL(website);
                        }, 3000)
                       
                    })
                    .catch(error => {
                        console.log(error)
                    })
            } catch (error) {
                console.log(error)
            }
        }else{
            Linking.openURL(website);
        }
    }

	fetchContent = () =>{
		fetch('http://api.hotelwaze.com/index/getmycountryinfo', {
	        method: 'POST',
	        headers: {
	            Accept: 'application/json',
	        },
	        body: JSON.stringify({
	            apikey: '1'
	        }),
	    })
	    .then((response) => response.json())
	    .then((responseJson) => {
	    	global.myCountryNamex = responseJson.data.countryName;
	        global.countryx = responseJson.data.countryId;
	    	this.fetchBanner();
	    })
	    .catch((error) => {
	        console.error(error);

	    })
	};

	fetchBanner = () =>{
		fetch('http://api.hotelwaze.com/index/banner', {
	        method: 'POST',
	        headers: {
	            Accept: 'application/json',
	        },
	        body: JSON.stringify({
	            apikey: '1'
	        }),
	    })
	    .then((response) => response.json())
	    .then((responseJson) => {
	    	this.setState({
                data:responseJson.data,
            });
            this.toptenFetch();
	    })
	    .catch((error) => {
	        console.error(error);

	    })
	};

	_handleAccount = () =>{
		if(tokenx == ''){
			this.props.navigation.navigate('SignIn');
		}else{
			this.props.navigation.navigate('Account');
		}
	};

	CheckConnectivity = () => {
		// For Android devices
		if (Platform.OS === "android") {
			NetInfo.isConnected.fetch().then(isConnected => {
				if (isConnected) {
					this.fetchContent();
				} else {					
				    this.props.navigation.navigate('NoConnection');
				}
			});
		} else {
			// For iOS devices
			NetInfo.isConnected.addEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
			);
		}
	};

	handleFirstConnectivityChange = isConnected => {
		NetInfo.isConnected.removeEventListener(
			"connectionChange",
			this.handleFirstConnectivityChange
		);

		if (isConnected === false) {
			this.props.navigation.navigate('NoConnection');
		} else {
			this.fetchContent();
		}
	};

	toptenFetch = () =>{
		fetch('http://api.hotelwaze.com/index/searchtopten', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                countryId:countryx
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
              datax:responseJson.data,
              loadingMore: true,
            });
        })

        .catch((error) => {
            console.error(error);
            this.setState({ loading: false })
        })
	}

	passCordinates = (latitude, longitude, name, phone, address1, website, hotelID, permission) =>{
        global.latitudex = parseFloat(latitude);
        global.longitudex = parseFloat(longitude);
        global.hotelNamex = name;
        global.hotelPhonex = phone;
        global.hotelAddress1x = address1;
        global.hotelWebsitex = website;
        global.hotelIDx = hotelID;
        global.permissionx = permission;
        this.props.navigation.navigate('SingleSearchItem');
    };

    passScreenName = (name, screenName, hotelID) =>{
    	global.hotelNamex = name;
    	global.screenNamex = screenName;
    	global.hotelIDx = hotelID;
    	this.props.navigation.navigate('Enquiry');
    }

	renderPage(image, index) {
        return (
            <View key={index}>
                <Image style={{ width: BannerWidth, height: BannerHeight }} source={{ uri: image['images'] }} />
            </View>
        );
    }

	render(){
		const imagex = this.state.data;

		return(
			<Container>
				<StatusBar hidden />
				<Content>
					<View style={{height: 253, flex: 1, backgroundColor:'#000066', justifyContent:'center'}}>
						<Carousel
		                    autoplay
		                    autoplayTimeout={6000}
		                    loop
		                    showsPageIndicator
		                    pageIndicatorContainerStyle={{bottom: 30}}
		                    activePageIndicatorStyle={{backgroundColor:'#000066'}}
		                    index={0}
		                    pageSize={BannerWidth}
		                >
                    		{imagex.map((image, index) => this.renderPage(image, index))}
                		</Carousel>
					</View>
					<View style={styles.container}>
						<TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Search')}>
							<Row style={styles.searchButton}>
								<Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
								<Text style={{paddingLeft: 5, color: '#707070'}}>Search for nearby hotel</Text>
							</Row>
						</TouchableOpacity>
					</View>
					{/**Google Adds**/}
					<View style={{width: '100%', display:'flex', alignItems:'center', justifyContent:'center', paddingBottom: 20}}>
						<AdMobBanner
						  	bannerSize="banner"
						  	adUnitID="ca-app-pub-8722731472004532/8016248635" // Test ID, Replace with your-admob-unit-id
						  	testDeviceID="EMULATOR"
						  	servePersonalizedAds // true or false
						  	onDidFailToReceiveAdWithError={this.bannerError} 
						/>
					</View>
					{/**Content**/}
					<View style={{paddingHorizontal: 20}}>
						<Text style={styles.searchResultTitlex}>Top 10 Hotels</Text>
					</View>
					<FlatList
	                    data={this.state.datax}
	                    extraData={this.state}  
	                    renderItem={
	                        ({item, index}) =>
	                        <View style={{paddingTop: 0,backgroundColor: '#fff'}}>
	                            <View>
	                                <View key={item.hotel_id} style={styles.searchResultContainer}>
	                                    <TouchableHighlight onPress={()=> 
	                                        this.passCordinates(
	                                            item.latitude, 
	                                            item.longitude, 
	                                            item.name, 
	                                            item.telephone_number,
	                                            item.address_1,
	                                            item.website,
	                                            item.hotel_id,
	                                            item.msg_permission
	                                        )
	                                    }>
	                                        <ImageBackground imageStyle={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }} 
	                                            source = {(item.logo_path != '') ? {uri: item.logo_path} : {uri: 'https://www.hotelwaze.com/assets/img/noavailable.jpg'}} 
	                                            style={{width: '100%', height: 126}}>
	                                        </ImageBackground>
	                                    </TouchableHighlight>
	                                    <View style={styles.searchResultInfo}>
	                                        <TouchableHighlight onPress={()=> 
	                                            this.passCordinates(
	                                                item.latitude, 
	                                                item.longitude, 
	                                                item.name, 
	                                                item.telephone_number,
	                                                item.address_1,
	                                                item.website,
	                                                item.hotel_id,
	                                                item.msg_permission
	                                            )
	                                        }>
	                                            <Text style={styles.searchResultTitle}>{item.name.replace('\r\n', '')}</Text>
	                                        </TouchableHighlight>
	                                        <TouchableHighlight style={{marginTop:10}} onPress={()=> 
	                                            this.passCordinates(
	                                                item.latitude, 
	                                                item.longitude, 
	                                                item.name, 
	                                                item.telephone_number,
	                                                item.address_1,
	                                                item.website,
	                                                item.hotel_id,
	                                                item.msg_permission
	                                            )
	                                        }>
		                                        <Row style={styles.searcResultTitleContainer}>
		                                            <FontAwesomeIcon style={styles.searhResultInfoIcon} icon={faMapMarkerAlt} />
		                                            <Text style={styles.searchResultInfoTitle}>{item.address_1.replace('\r\n', '')}</Text>
		                                        </Row>
	                                        </TouchableHighlight>
	                                        <Row style={{paddingTop: 15, paddingBottom: 5}}>
	                                        	<Col style={{paddingRight: 15}}>
			                                        <TouchableOpacity style={styles.contactInfo} onPress={()=>{Linking.openURL(`tel:${item.telephone_number}`)}}>
			                                            <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
				                                            <View style={{padding: 15}}>
				                                            	<View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
				                                            		<View style={{paddingBottom: 5}}>
					                                                	<FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faPhone} />
					                                                </View>
					                                                <Text style={styles.searchResultInfoTitlex}>Contact</Text>
					                                                <Text style={styles.searchResultInfoTitlex}>Hotel</Text>
				                                                </View>
				                                            </View>
			                                            </View>
			                                        </TouchableOpacity>
		                                        </Col>
		                                        <Col style={{paddingRight: 15}}>
		                                        	{item.msg_permission == 1 ?
		                                        		<TouchableOpacity style={styles.contactInfo}
				                                        	onPress={() =>
				                                        		this.passScreenName(
				                                        			item.name,
				                                        			'homeScreenx',
				                                        			item.hotel_id
				                                        		)
				                                        }>
				                                            <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
					                                            <View style={{padding: 15}}>
					                                            	<View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
					                                            		<View style={{paddingBottom: 5}}>
					                                                		<FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faEnvelope} />
					                                                	</View>
					                                                	<Text style={styles.searchResultInfoTitlex}>Enquire</Text>
					                                                	<Text style={styles.searchResultInfoTitlex}>Hotel</Text>
					                                                </View>
					                                            </View>
					                                        </View>
				                                        </TouchableOpacity>
		                               
				                                        : 
				                                       	
				                                       	<TouchableOpacity style={styles.contactInfo} disabled={true}>
				                                            <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
					                                            <View style={{padding: 15}}>
					                                            	<View style={{display:'flex', alignItems:'center',justifyContent:'center',backgroundColor: '#fff', opacity:0.4}}>
					                                            		<View style={{paddingBottom: 5}}>
					                                                		<FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faEnvelope} />
					                                                	</View>
					                                                	<Text style={styles.searchResultInfoTitlex}>Enquire</Text>
					                                                	<Text style={styles.searchResultInfoTitlex}>Hotel</Text>
					                                                </View>
					                                            </View>
					                                        </View>
				                                        </TouchableOpacity>
				                                    }
		                                        </Col>
		                                        <Col style={{paddingRight: 15}}>
			                                        <TouchableOpacity style={styles.contactInfo} onPress={()=>{this.handlePlay('one',item.website)}}>
			                                            <View style={{backgroundColor: '#fff', borderBottomLeftRadius:3,borderBottomRightRadius:3}}>
				                                            <View style={{padding: 15}}>
				                                            	<View style={{display:'flex', alignItems:'center',justifyContent:'center'}}>
				                                            		<View style={{paddingBottom: 5}}>
				                                                		<FontAwesomeIcon size={18} style={styles.searhResultInfoIconx} icon={faGlobe} />
				                                                	</View>
				                                                	<Text style={styles.searchResultInfoTitlex}>Hotel</Text>
				                                                	<Text style={styles.searchResultInfoTitlex}>Website</Text>
				                                                </View>
				                                            </View>
				                                        </View>
			                                        </TouchableOpacity>
		                                        </Col>
		                                        <Col>
		                                        	
		                                        </Col>
	                                        </Row>
	                                    </View>
	                                </View>
	                            </View>
	                        </View>
	                    }
	                    keyExtractor={item => item.hotel_id}
	                    onEndReachedThreshold={5}
	                    initialNumToRender={10}
	                    refreshing={this.state.refreshing}
	                />
				</Content>
				<Footer>
					<FooterTab style={styles.footerContainer}>
						<Button  onPress={() => this.props.navigation.push('Home')}>
							<FontAwesomeIcon style={styles.navActiveIcon} icon={faHome} />
							<Text style={styles.navActiveText} >Home</Text>
						</Button>
						<Button onPress={() => this.props.navigation.push('Search')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faSearch} />
							<Text style={styles.navInactiveText} >Search</Text>
						</Button>
						<Button onPress={() => this.props.navigation.push('ContactUs')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faEnvelope} />
							<Text style={styles.navInactiveText} >Contact Us</Text>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}


const styles = StyleSheet.create({
	view:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'white'
	},
	text:{
		fontSize: 26,
		color: 'purple'
	},
	touchableHighlight:{
		width: 50,
		height: 50,
		backgroundColor: 'red'
	},
	container:{
      paddingHorizontal: 10,
      height: 45,
   },
   textContainer:{
      paddingTop: 10,
      paddingHorizontal: 10,
   },
   title:{
      fontSize: 18,
      fontWeight: 'bold',
      color: '#000066'
   },
   textTitle:{
      fontSize: 12,
      fontWeight: 'bold',
      color: '#000066'
   },
   text:{
      paddingTop: 5,
      paddingBottom: 5,
      fontSize: 10,
      color: '#000066'
   },
   searchButton:{
      height: 45,
      top: -20,
      alignItems:'center',
      paddingHorizontal: 10,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
   },
   searchIcon: {
      //padding: 10,
      marginLeft: 0,
      color: '#707070',
   },
   input: {
      flex: 1,
      paddingTop: 10,
      paddingRight: 10,
      paddingBottom: 10,
      paddingLeft: 0,
      backgroundColor: '#fff',
      color: '#707070',
      borderRadius: 10,
      fontSize: 20
   },
   searchResultContainer:{
        flex: 1, 
        marginVertical: 10,
        marginHorizontal: 20,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 5,
    },
    contactInfo:{
    	display:'flex', 
    	alignItems:'center', 
    	justifyContent:'center',
    	shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height:2,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    searchResultInfoTitlex:{
    	textAlign: 'center',
        color: '#003580',
        fontSize: 8,
        paddingLeft: 5,
    },
    searhResultInfoIconx:{
        color: '#003580',
        fontSize: 31,
    },
    searchResultInfo:{
        padding: 10,
    },
    searchResultTitlex:{
    	color: '#003580',
    	fontSize: 14,
    	fontWeight: 'bold',
    	zIndex:99
    },
    searchResultTitle:{
        color: '#003580',
        fontSize: 18,
        fontWeight: 'bold',
    },
    searcResultTitleContainer:{
        paddingTop: 5,
    },
    searcResultTitleContainer:{
    	textAlign: 'center',
    },
    searhResultInfoIcon:{
        color: '#003580',
        fontSize: 10,
    },
    searchResultInfoTitle:{
        color: '#003580',
        fontSize: 12,
        paddingLeft: 5,
    },
   footerContainer:{
		backgroundColor: '#fff',
	},
	navActiveIcon:{
		fontSize: 20,
		color: '#000066',
	},
	navActiveText:{
		fontSize: 14,
		color: '#000066',
	},
	navInactiveIcon:{
		fontSize: 20,
		color: '#a5a5a5',
	},
	navInactiveText:{
		fontSize: 14,
		color: '#a5a5a5',
	}
});
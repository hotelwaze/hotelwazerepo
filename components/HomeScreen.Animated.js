import React, { Component } from 'react';
import {Animated, Platform, StatusBar, StyleSheet, Text, View, RefreshControl, TouchableOpacity} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon, Row, Col} from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHome, faUser, faSearch, faEnvelope } from '@fortawesome/free-solid-svg-icons'

const HEADER_MAX_HEIGHT = 253;
const HEADER_MIN_HEIGHT = 65;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
            // iOS has negative initial scroll value because content inset...
            Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
        ),
        refreshing: false,
        };
    }

    _renderScrollViewContent() {
        const data = Array.from({ length: 30 });
        return (
            <View style={{paddingTop: 30}}>
                <View style={styles.scrollViewContent}>
                   <View style={styles.container}>
                        <Text style={styles.titlex}>Top 7 main reasons to book directly with hotels.</Text>
                    </View>
                    <View style={styles.textContainer}>
                        <Text style={styles.textTitle}>1. Get Better Rates</Text>
                        <Text style={styles.text}>Your hotel may offer discounts and special rates if you book directly with them. Just ask them for offers!</Text>
                        <Text style={styles.textTitle}>2. Cancel Anytime</Text>
                        <Text style={styles.text}>Some hotels will let you cancel your booking without charging a no-show or a cancellation fee, unlike third party booking sites and services. But don’t forget to ask the hotel about their cancellation policies.</Text>
                        <Text style={styles.textTitle}>3. Enjoy Special Occasions</Text>
                        <Text style={styles.text}>Most hotels will do their best to accommodate your special occasion if you book directly with them. Make your stay even more special!</Text>
                        <Text style={styles.textTitle}>4. Change Things Around</Text>
                        <Text style={styles.text}>Most hotels will make amendments to your booking without any difficulties, as long as what you want is available.</Text>
                        <Text style={styles.textTitle}>5. Get Rewards</Text>
                        <Text style={styles.text}>Ask your hotel about reward points, which are often available to patrons when they book directly with the hotel. Reward programs often include discounts and freebies.</Text>
                        <Text style={styles.textTitle}>6. Get Discounts on an Extra Room</Text>
                        <Text style={styles.text}>Most hotels offer discounts for booking an extra room on the same day if you call late afternoon. Just ask the front desk.</Text>
                        <Text style={styles.textTitle}>7. Relocate With Ease</Text>
                        <Text style={styles.text}>It’s easier to ask a hotel if you want to be relocated to another hotel or to another room if you use direct booking.</Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );

        const headMov = this.state.scrollY.interpolate({
          inputRange: [0, 180, 181],
          outputRange: [0, -35, -35]
        });

        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });

        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const searchPosition = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });


        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });

        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -8],
            extrapolate: 'clamp',
        });

        return (
            <View style={styles.fill}>
                <StatusBar hidden/>
                <Animated.ScrollView 
                    style={styles.fill}
                    scrollEventThrottle={1}
                    onScroll={
                        Animated.event(
                            [
                                { 
                                    nativeEvent: { 
                                        contentOffset: { 
                                            y: this.state.scrollY 
                                        } 
                                    } 
                                }
                            ],
                            { 
                                useNativeDriver: true 
                            },
                        )
                    }
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => 
                                {
                                    this.setState({ refreshing: true });
                                    setTimeout(() => this.setState({ refreshing: false }), 1000);
                                }
                            }
                            // Android offset for RefreshControl
                            progressViewOffset={HEADER_MAX_HEIGHT}
                        />
                    }
                    // iOS offset for RefreshControl
                    contentInset={{
                        top: HEADER_MAX_HEIGHT,
                    }}
                    contentOffset={{
                        y: -HEADER_MAX_HEIGHT,
                    }}
                >
                    {
                        this._renderScrollViewContent()
                    }
                </Animated.ScrollView>
                <Animated.View
                    pointerEvents="none"
                    style={[
                        styles.header,
                        { 
                            transform: [{ 
                                translateY: headerTranslate 
                            }] 
                        },
                    ]}
                >

                    <Animated.Image
                        style={[
                            styles.backgroundImage,
                            {
                                opacity: imageOpacity,
                                transform: [{ 
                                    translateY: imageTranslate 
                                }],
                            },
                        ]}
                        source={require('../assets/wazeBg.png')}
                    />
                </Animated.View>

                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                { 
                                    translateY: headerTranslate 
                                },
                            ],
                        },
                    ]}
                >
                    <View style={{position:'absolute', top: 0, zIndex: 999}}>
                        <Button transparent style={{width: 50}} onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}>
                            <Icon name='menu' style={{color: '#fff'}}></Icon>
                        </Button>
                    </View>
                    <Animated.View 
                        style={[
                            styles.title,
                            {
                                opacity: imageOpacity
                            }
                        ]}
                    >
                        <Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold' }}>Welcome</Text>
                        <Text style={{color: '#fff', fontSize: 24, fontWeight: 'bold'}}>Search your favorite hotel now!</Text>
                    </Animated.View>
                    
                    <View>
                        <View style={styles.container}>
                            <Animated.View
                                style={[
                                    styles.searchButton,
                                    {
                                        translateY: headMov
                                    }
                                ]}
                            >
                                <TouchableOpacity style={{flex: 1, zIndex: 9999, width: '100%'}} onPress={() => this.props.navigation.navigate('Search')}>
                                    <Row style={{flex: 1, width: '100%', alignItems: 'center'}}>
                                        <Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
                                        <Text style={{paddingLeft: 5, color: '#707070'}}>Search for nearby hotel</Text>
                                    </Row>
                                </TouchableOpacity>
                            </Animated.View>
                        </View>  
                    </View>
                </Animated.View>
                <Footer>
                    <FooterTab style={styles.footerContainer}>
                        <Button  onPress={() => this.props.navigation.navigate('Home')}>
                            <FontAwesomeIcon style={styles.navActiveIcon} icon={faHome} />
                            <Text style={styles.navActiveText} >Home</Text>
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('Search')}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faSearch} />
                            <Text style={styles.navInactiveText} >Search</Text>
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('Account')}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faUser} />
                            <Text style={styles.navInactiveText} >Account</Text>
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('ContactUs')}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faEnvelope} />
                            <Text style={styles.navInactiveText} >Contact Us</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fill: {
        flex: 1,
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#006CFF',
        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        //marginTop: Platform.OS === 'ios' ? 28 : 38,
        //height: 32,
        //alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: HEADER_MAX_HEIGHT
    },
    title: {
        justifyContent: 'center',
        height: HEADER_MAX_HEIGHT,
        padding: 10,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.

        //paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
        paddingTop: HEADER_MAX_HEIGHT,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container:{
        paddingHorizontal: 10,
    },
    searchButton:{
      height: 45,
      top: -20,
      alignItems:'center',
      paddingHorizontal: 10,
      backgroundColor: '#fff',
      borderRadius: 10,
      shadowColor: "#000",
      shadowOffset: {
         width: 0,
         height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
   },
   searchIcon: {
      //padding: 10,
      marginLeft: 0,
      color: '#707070',
   },
   footerContainer:{
        backgroundColor: '#fff',
    },
    navActiveIcon:{
        fontSize: 20,
        color: '#006CFF',
    },
    navActiveText:{
        fontSize: 14,
        color: '#006CFF',
    },
    navInactiveIcon:{
        fontSize: 20,
        color: '#a5a5a5',
    },
    navInactiveText:{
        fontSize: 14,
        color: '#a5a5a5',
    },
    textContainer:{
      paddingTop: 10,
      paddingHorizontal: 10,
    },
    titlex:{
      fontSize: 18,
      fontWeight: 'bold',
   },
   textTitle:{
      fontSize: 12,
      fontWeight: 'bold',
   },
   text:{
      paddingTop: 5,
      paddingBottom: 5,
      fontSize: 10
   },
});
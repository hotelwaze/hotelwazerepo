import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem } from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

export default class SelectCountry extends Component{
	_signIn = () =>{
		fetch('http://api.hotelwaze.com/index/allcountry', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				apikey: '1',
			}),
		})
		.then((response) => response.json())
		.then((responseJson) => {
			
		})
		.catch((error) => {
			console.error(error);
		})
	}
	render(){
		return(
			<Container>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Search')}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>Select Country</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content>
					<View style={styles.searchContent}>
						<Text style={styles.title}>Country</Text>
						<View styles={styles.searchInput}>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
					</View>
					<List style={{paddingHorizontal: 10, paddingTop: 10}}>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Afghanistan</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Albania</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Algeria</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>American Samoa</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Andorra</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Angola</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Anguilla</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Antigua & Barbuda</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Argentina</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Armenia</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Aruba</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Australia</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Austria</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Azerbaijan</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Bahamas</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Bahrain</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Bangladesh</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Barbados</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Belarus</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
						<ListItem style={{marginLeft: 0}}>
							<Row>
								<Col>
									<Text>Belgium</Text>
								</Col>
								<View>
									<FontAwesomeIcon style={{color: '#a5a5a5'}} icon={faArrowRight} />
								</View>
							</Row>
						</ListItem>
					</List>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({	
	container:{
		padding: 10,
	},
	header:{
		alignItems: 'center',
		backgroundColor: '#fff',
		elevation: 0,
	},
	searchContent:{
		flex: 1,
		borderRadius: 5,
		paddingHorizontal: 10,
		paddingVertical: 20,
		margin: 10,
		backgroundColor: '#fff',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
	},
	title:{
		fontSize: 10,
		color: '#a5a5a5',
		paddingBottom: 5,
	},
	searchInput:{
		backgroundColor: '#f1f1f1',
		position: 'absolute', 
		top: 0,
		left: 0,
		right: 0,
	},
	input:{
		height: 30,
        padding: 10,
        fontSize: 10,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
	},
	
});
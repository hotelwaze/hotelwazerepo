import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity, Image, Alert} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem, Button} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

export default class PaymentScreen extends Component{
	
	_registered = () =>{
		Alert.alert(
			'Registration Complete',
			'You are now registered',
			[
				{
					text: 'OK', onPress: () => this.props.navigation.navigate('Account')
				},
			],
			{
				cancelable: false
			},
		);
	}

	render(){
		return(
			<Container>
				<StatusBar hidden/>
				<Header style={styles.header}>
					<TouchableOpacity style={{flex: 1}}onPress={() => this.props.navigation.navigate('Register')}>
						<Row style={{alignItems: 'center'}}>
							<View style={{paddingRight: 20}}>
								<FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
							</View>
							<Col>
								<Text style={{fontSize: 14}}>Payment</Text>
							</Col>
						</Row>
					</TouchableOpacity>
				</Header>
				<Content style={{paddingHorizontal: 10}}>
					<View style={styles.boxContainer}>
						<View style={{padding: 10}}>
							<Text style={{fontSize: 14, fontWeight: 'bold'}}>Activate your Free Trial</Text>
							<View style={{paddingTop: 10}}>
								<View style={{paddingBottom: 10}}>
									<Text style={{fontSize: 12, color:'#003580'}}>Hotel Search Unlimited - Free Trial fo 1 Year</Text>
									<Text style={{fontSize: 8, color:'#003580'}}>RENEW AT $1/YEAR</Text>
								</View>
								<View style={{borderTopWidth: 0.5, borderBottomWidth: 0.5, borderColor:'#707070', paddingVertical: 10}}>
									<Row>
										<Text style={{fontSize: 8, fontWeight: 'bold', color:'#003580'}}>Today You Pay</Text>
										<Text style={{paddingLeft: 20, fontSize: 8, fontWeight: 'bold', color:'#003580'}}>$0.00</Text>
									</Row>
								</View>
								<View style={{paddingTop: 10}}>
									<Text style={{fontSize: 10}}>After your Free Trial... We'll keep your phone number active by automatically renewing you to a yearly plan on 2020-08-24 you will bill $1.00 plus applicable taxes.</Text>
								</View>
							</View>
						</View>
					</View>
					<View style={{padding: 10}}>
						<View>
							<Image source={require('../assets/cards.png')} />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Name on the Card</Text>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
						<View style={{paddingBottom: 10}}>
							<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Card Number</Text>
							<TextInput style={styles.input} editable maxLenght="100" />
						</View>
						<View style={{paddingBottom: 10}}>
							<Row>
								<Col>
									<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Expiration Month</Text>
									<TextInput style={styles.input} editable maxLenght="100" placeholder="MM"/>
								</Col>
								<Col style={{paddingHorizontal: 10}}>
									<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>Expiration Year</Text>
									<TextInput style={styles.input} editable maxLenght="100" placeholder="YYYY"/>
								</Col>
								<Col>
									<Text style={{fontSize: 10, color: '#a5a5a5', paddingBottom: 5}}>CVC</Text>
									<TextInput style={styles.input} editable maxLenght="100" placeholder="ex: 311"/>
								</Col>
							</Row>
						</View>
						<Row style={{alignItems: 'center'}}>
							<Col>
								<Button transparent onPress={() => this.props.navigation.navigate('Terms')}>
									<Text style={{color: '#006CFF', fontSize: 12}}>Why Do We need this?</Text>
								</Button>
							</Col>
							<Col style={{alignItems: 'flex-end'}}>
								<Button style={{width: 105, height: 30, borderRadius: 15, backgroundColor: '#28A745'}} onPress={()=> this._registered()}>
									<Text style={{textAlign: 'center', width: '100%', color: '#fff', fontSize: 10}}>START FREE TRIAL</Text>
								</Button>
							</Col>
						</Row>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({	
	container:{
		padding: 10,
	},
	header:{
		alignItems: 'center',
		backgroundColor: '#fff',
		elevation: 0,
	},
	boxContainer:{
		flex: 1,
		borderRadius: 5,
		margin: 10,
		backgroundColor: '#fff',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3,
	},
	title:{
		fontSize: 10,
		color: '#a5a5a5',
		paddingBottom: 5,
	},
	searchInput:{
		backgroundColor: '#f1f1f1',
		position: 'absolute', 
		top: 0,
		left: 0,
		right: 0,
	},
	input:{
		height: 30,
        padding: 10,
        fontSize: 10,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
	},
	list:{
		paddingTop: 15
	},
	listItem:{
		paddingHorizontal: 15,
		paddingVertical: 0,
		height: 30,
		backgroundColor: '#fff',
		borderRadius: 15,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,
		elevation: 3
	},
	listItemContent:{
		alignItems: 'center'
	},
	listItemText:{
		fontSize: 14,
		color: '#A5A5A5'
	},
	listItemIcon:{
		fontSize: 18,
		color: '#A5A5A5'
	},
	
});
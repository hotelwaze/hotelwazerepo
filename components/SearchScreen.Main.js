import React, { Component } from 'react';
import {Animated, Platform, StatusBar, StyleSheet, Text, View, RefreshControl, TouchableOpacity, Linking, ImageBackground, TextInput, ScrollView} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon, Row, Col} from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faMapMarkerAlt, faPhone, faGlobe, faHome, faUser, faSearch, faEnvelope } from '@fortawesome/free-solid-svg-icons'

const HEADER_MAX_HEIGHT = 210;
const HEADER_MIN_HEIGHT = 65;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

global.latitudex = '';
global.longitudex = '';
global.hotelNamex = '';
global.hotelPhonex = '';
global.hotelAddress1x = '';
global.hotelWebsitex = '';
global.activateCity = '';
global.acivate = '';
global.statex = '';
global.cityx = '';



export default class SearchScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            current_page: 0,
            error: null,
            hasMore: true,
            searchKeyword:'',
            scrollY: new Animated.Value(
            // iOS has negative initial scroll value because content inset...
            Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
        ),
        refreshing: false,
        };
        
    };


/*    getListOfPictures = () => {
        if (this.state.loading) { return; }
        this.setState({ loading: true });
        API.get(`/pictures?page=${this.state.current_page}`)
        .then(res => {
            const nextPictures= res.data.pictures.data.map(data => ({
            title: data.title,
            image_url: data.image_url,
            description: data.description,
            id: data.id
        }));
        this.setState({
            hasMore: ( res.data.pictures.current_page <=         
            res.data.pictures.last_page),        
            data: [...this.state.data, ...nextPictures],
            loading: false,
            current_page: this.state.current_page + 1})
        })
        .catch(error => {
            this.setState({ error, loading: false });
        });
    }*/

    _scrollMore= () =>{
        if (this.state.loading) { 
             return; 
        } 
        let currentPage = parseInt(this.state.current_page)+ 1;
        this.setState({ 
            loading: true,
            current_page: currentPage
        });

        let searchKeyword = this.state.searchKeyword;
        fetch('http://api.hotelwaze.com/index/searchHotel', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                token: tokenx,
                keywords: searchKeyword,
                country_id: countryx,
                state_id: statex,
                city_id: cityx,
                page: currentPage
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                data: responseJson.data
            })
            console.log(responseJson.data)
        })
        .catch((error) => {
            console.error(error);
        })
    }

   _handleSearch = () =>{
        /*if(tokenx == ''){
            this.props.navigation.navigate('SignIn');
        }else{
            let searchKeyword = this.state.searchKeyword;
            fetch('http://api.hotelwaze.com/index/searchHotel', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    apikey: '1',
                    token: tokenx,
                    keywords: searchKeyword,
                    country_id: countryx,
                    state_id: statex,
                    city_id: cityx,
                    page: 0
                }),
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    data:responseJson.data,
                })
            })
            .catch((error) => {
                console.error(error);
            })
        }*/
        let searchKeyword = this.state.searchKeyword;
        fetch('http://api.hotelwaze.com/index/searchHotel', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                token: tokenx,
                keywords: searchKeyword,
                country_id: countryx,
                state_id: statex,
                city_id: cityx,
                page: 0
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                data:responseJson.data,
            })
        })
        .catch((error) => {
            console.error(error);
        })
    };

    _handleAccount = () =>{
        if(tokenx == ''){
            this.props.navigation.navigate('SignIn')
        }else{
            this.props.navigation.navigate('Account');
        }
    };

    passCordinates = (latitude, longitude, name, phone, address1, website) =>{
        global.latitudex = parseFloat(latitude);
        global.longitudex = parseFloat(longitude);
        global.hotelNamex = name;
        global.hotelPhonex = phone;
        global.hotelAddress1x = address1;
        global.hotelWebsitex = website;
        this.props.navigation.navigate('SingleSearchItem');

    };

    componentDidMount(){
        fetch('http://api.hotelwaze.com/index/searchtopten', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                countryId:countryx
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
              data:responseJson.data,
            })
        })
        .catch((error) => {
            console.error(error);
        })
    }

    updateCountry = () =>{
        global.acivate = '1';
        this.props.navigation.navigate('SelectCountry');
    }

       
    _renderScrollViewContent() {
        return (
            <View style={{paddingTop: 30, backgroundColor: '#fff'}}>
                <View style={styles.scrollViewContent}>
                    {
                        this.state.data.map((hotel, index) => (
                            <View key={hotel.hotel_id} style={styles.searchResultContainer}>
                                <TouchableOpacity onPress={()=> 
                                    this.passCordinates(
                                        hotel.latitude, 
                                        hotel.longitude, 
                                        hotel.name, 
                                        hotel.telephone_number,
                                        hotel.address_1,
                                        hotel.website
                                    )
                                }>
                                    <ImageBackground imageStyle={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }} 
                                        source = {(hotel.logo_path != '') ? {uri: hotel.logo_path} : {uri: 'https://www.hotelwaze.com/assets/img/noavailable.jpg'}} 
                                        style={{width: '100%', height: 126}}>
                                    </ImageBackground>
                                </TouchableOpacity>
                                <View style={styles.searchResultInfo}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('SingleSearchItem')}>
                                        <Text style={styles.searchResultTitle}>{hotel.name.replace('\r\n', '')}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('SingleSearchItem')}>
                                        <Row style={styles.searcResultTitleContainer}>
                                            <FontAwesomeIcon style={styles.searhResultInfoIcon} icon={faMapMarkerAlt} />
                                            <Text style={styles.searchResultInfoTitle}>{hotel.address_1.replace('\r\n', '')}</Text>
                                        </Row>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>{Linking.openURL(`tel:${hotel.telephone_number}`)}}>
                                        <Row style={styles.searcResultTitleContainer}>
                                            <FontAwesomeIcon style={styles.searhResultInfoIcon} icon={faPhone} />
                                            <Text style={styles.searchResultInfoTitle}>{hotel.telephone_number == null ? 'N/A' : hotel.telephone_number.replace('\r\n', '') }</Text>
                                        </Row>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>{Linking.openURL(hotel.website)}}>
                                        <Row style={styles.searcResultTitleContainer}>
                                            <FontAwesomeIcon style={styles.searhResultInfoIcon} icon={faGlobe} />
                                            <Text style={styles.searchResultInfoTitle}>View Website</Text>
                                        </Row>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        ))
                    }
                  </View>
            </View>
        );
    }

   

    render() {
        const { navigation } = this.props;

        global.keywordx = navigation.getParam('keywordName', '');
        global.countryx = navigation.getParam('countryId', countryx);
        global.statex = navigation.getParam('stateId', '');
        global.cityx = navigation.getParam('cityId', '');

        if(acivate == '1'){
            global.stateNamex = 'Select State';
            global.cityNamex = 'Select City';
            global.statex = '';
            global.cityx = '';
        }else{
            global.stateNamex = navigation.getParam('stateName', 'Select State');
            if(activateCity == '1'){
                global.cityNamex = 'Select City';
                global.cityx = '';
            }else{
                global.cityNamex = navigation.getParam('cityName', 'Select City');
            }
        }

        console.log('countryID: '+countryx);

        console.log('activate: '+acivate);

        console.log('activateCity: '+activateCity);

        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );

        const headMov = scrollY.interpolate({
          inputRange: [0, 180, 181],
          outputRange: [0, -35, -35]
        });

        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });

        const imageOpacityx = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, 1],
            extrapolate: 'clamp',
        });

        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const searchPosition = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });


        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });

        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -8],
            extrapolate: 'clamp',
        });

        
        
        return (
            <View style={styles.fill}>
                <StatusBar hidden/>
                <Animated.ScrollView 
                    style={styles.fill}
                    scrollEventThrottle={400}
                    onScroll={({nativeEvent}) => {
                      if (isCloseToBottom(nativeEvent)) {
                        this._scrollMore();
                      }
                    }}
                        
                        /*Animated.event(
                            [
                                { 
                                    nativeEvent: { 
                                        contentOffset: { 
                                            y: this.state.scrollY 
                                        } 
                                    } 
                                }
                            ],
                            { 
                                useNativeDriver: true 
                            },
                        )*/
                    
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => 
                                {
                                    this.setState({ refreshing: true });
                                    setTimeout(() => this.setState({ refreshing: false }), 1000);
                                }
                            }
                            // Android offset for RefreshControl
                            progressViewOffset={HEADER_MAX_HEIGHT}
                        />
                    }
                    // iOS offset for RefreshControl
                    contentInset={{
                        top: HEADER_MAX_HEIGHT,
                    }}
                    contentOffset={{
                        y: -HEADER_MAX_HEIGHT,
                    }}
                >
                    {
                        this._renderScrollViewContent()
                    }
                </Animated.ScrollView>
                <Animated.View
                    pointerEvents="none"
                    style={[
                        styles.header,
                        { 
                            transform: [{ 
                                translateY: headerTranslate 
                            }] 
                        },
                    ]}
                >

                </Animated.View>
                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                { 
                                    translateY: headerTranslate 
                                },
                            ],
                        },
                    ]}
                >
                    <Animated.View 
                        style={[
                            styles.title,
                            {
                                opacity: imageOpacity
                            }
                        ]}
                    >
                        <View style={styles.searchContent}>
                            <Text style={{fontSize: 10, paddingBottom: 5}}>Keyword</Text>
                            <View styles={styles.searchInput}>
                                {/*<TextInput 
                                    placeholderTextColor="#C1C1C1" 
                                    style={styles.input} placeholder='' 
                                    onChangeText={(searchKeyword) => this.setState({searchKeyword})} 
                                    value={this.state.searchKeyword} 
                                />*/}
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('SelectKeyword')}>
                                    <View style={{backgroundColor: '#f1f1f1', height: 30, borderRadius: 15, justifyContent: 'center', paddingHorizontal: 5}}>
                                        <Text style={styles.searchFilterTitleMain}>{navigation.getParam('keywordName', 'Search Keyword')}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1, paddingTop:20}}>
                                <Row style={styles.searchInfo}>
                                    <Col>
                                        <TouchableOpacity onPress={() => this.updateCountry()}>
                                            <View style={{borderRightWidth: 0.5, borderRightColor: '#f1f1f1', marginRight: 15}}>
                                                <Text style={{fontSize: 10}}>Country</Text>
                                                <Text style={styles.searchFilterTitle}>{navigation.getParam('countryName', myCountryNamex)}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Col>
                                    <Col>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SelectState')}>
                                            <View style={{borderRightWidth: 0.5, borderRightColor: '#f1f1f1', marginRight: 15}}>
                                                <Text style={{fontSize: 10}}>State</Text>
                                                <Text style={styles.searchFilterTitle}>{stateNamex}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Col>
                                        <Col>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SelectCity')}>
                                            <View style={{marginRight: 15}}>
                                                <Text style={{fontSize: 10}}>City</Text>
                                                <Text style={styles.searchFilterTitle}>{cityNamex}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                            </View>
                            <View style={{flex: 1, paddingTop: 10}}>
                                <TouchableOpacity style={styles.buttonSearch} onPress={()=>this._handleSearch()}>
                                    <Text style={styles.buttonText}>SEARCH</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Animated.View>
                    <Animated.View
                        style={[
                            styles.searchButton,
                            {
                                opacity: imageOpacityx,
                                translateY: headMov
                            }
                        ]}
                    >
                        <View style={{flex: 1, paddingHorizontal: 10, width: '100%'}}>
                            <TouchableOpacity style={{flex: 1, zIndex: 9999, width: '100%'}}>
                                <Row style={{flex: 1, width: '100%', alignItems: 'center'}}>
                                    <Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
                                    <Text style={{paddingLeft: 5, color: '#707070'}}>Search for nearby hotel</Text>
                                </Row>
                            </TouchableOpacity>
                        </View>
                    </Animated.View>
                </Animated.View>
                <Footer>
                    <FooterTab style={styles.footerContainer}>
                        <Button  onPress={() => this.props.navigation.navigate('Home')}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faHome} />
                            <Text style={styles.navInactiveText} >Home</Text>
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate('Search')}>
                            <FontAwesomeIcon style={styles.navActiveIcon} icon={faSearch} />
                            <Text style={styles.navActiveText} >Search</Text>
                        </Button>
                        {/*<Button onPress={() => this._handleAccount()}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faUser} />
                            <Text style={styles.navInactiveText} >Account</Text>
                        </Button>*/}
                        <Button onPress={() => this.props.navigation.navigate('ContactUs')}>
                            <FontAwesomeIcon style={styles.navInactiveIcon} icon={faEnvelope} />
                            <Text style={styles.navInactiveText} >Contact Us</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fill: {
        flex: 1,
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#006CFF',
        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    bar:{
        backgroundColor: 'transparent',
        //marginTop: Platform.OS === 'ios' ? 28 : 38,
        //height: 32,
        //alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: HEADER_MAX_HEIGHT
    },
    title: {
        //justifyContent: 'center',
        height: HEADER_MAX_HEIGHT,
        backgroundColor: '#fff',
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        //paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
        paddingTop: HEADER_MAX_HEIGHT,
    },
    container:{
        paddingHorizontal: 10,
    },
    searchContent:{
        flex: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 20,
        margin: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    searchInput:{
        backgroundColor: '#f1f1f1',
        position: 'absolute', 
        top: 0,
        left: 0,
        right: 0,
    },
    input:{
        height: 30,
        padding: 10,
        fontSize: 10,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
    },
    searchInfo:{
        borderTopWidth: 1,
        borderTopColor: '#f1f1f1',
        borderBottomWidth: 1,
        borderBottomColor: '#f1f1f1',
        paddingVertical: 5,
    },
    searchFilterTitleMain:{
        color: '#c0c0c0',
        fontSize: 10
    },
    searchFilterTitle:{
        color: '#000',
        fontSize: 10,
        fontWeight: 'bold',
    },
    buttonSearch:{
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:5,
        height: 30,
        backgroundColor: '#006CFF', 
    },
    buttonText:{
        color: '#fff',
        fontSize: 10,
        fontWeight: 'bold',
    },
    titlex:{
        fontSize: 10,
        color: '#a5a5a5',
        paddingBottom: 5,
    },
   searchResultContainer:{
        flex: 1, 
        marginVertical: 20,
        marginHorizontal: 20,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 3,
        elevation: 3,
        borderRadius: 5,
    },
    searchResultInfo:{
        padding: 10,
    },
    searchResultTitle:{
        color: '#003580',
        fontSize: 18,
        fontWeight: 'bold',
    },
    searcResultTitleContainer:{
        paddingTop: 5,
    },
    searhResultInfoIcon:{
        color: '#003580',
        fontSize: 10,
    },
    searchResultInfoTitle:{
        color: '#003580',
        fontSize: 12,
        paddingLeft: 5,
    },
   footerContainer:{
        backgroundColor: '#fff',
    },
    navActiveIcon:{
        fontSize: 20,
        color: '#000066',
    },
    navActiveText:{
        fontSize: 14,
        color: '#000066',
    },
    navInactiveIcon:{
        fontSize: 20,
        color: '#a5a5a5',
    },
    navInactiveText:{
        fontSize: 14,
        color: '#a5a5a5',
    },
    textContainer:{
      paddingTop: 10,
      paddingHorizontal: 10,
    },
    titlex:{
      fontSize: 18,
      fontWeight: 'bold',
   },
   textTitle:{
      fontSize: 12,
      fontWeight: 'bold',
   },
   text:{
      paddingTop: 5,
      paddingBottom: 5,
      fontSize: 10
   },
   searchButton:{
        marginHorizontal: 10,
        height: 45,
        top: -20,
        alignItems:'center',
        paddingHorizontal: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
   },
   searchIcon: {
      marginLeft: 0,
      color: '#707070',
   },
});
import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity, Image} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem } from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { SearchableFlatList } from "react-native-searchable-list"; 

export default class SelectKeyword extends Component{
    constructor() {
        super();
        this.state = {
            data:[],
            searchTerm: "",
            searchAttribute: "country_name",
            ignoreCase: true,
            stateID: statex,
            cityID: cityx
        };
    }

    getKey = (text) =>{

       if(this.state.stateID == ""){
            this.setState({
                stateID: 'ALL'
            });
       }else{
            this.setState({
                stateID: statex
            });
       }

       if(this.state.cityID == ""){
            this.setState({
                cityID: 'ALL'
            });
       }else{
            this.setState({
                cityID: cityx
            });
       }
        fetch('http://api.hotelwaze.com/index/searchKeyword', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                apikey: '1',
                keywords: text,
                country_id: countryx,
                state_id: this.state.stateID,
                city_id: this.state.cityID,
                token: tokenx,
                page: 0
            }), 
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            this.setState({
              data:responseJson.data,
            })
        })
        .catch((error) => {
            console.error(error);
        })
    }

    submit = () =>{
        let val = this.state.text;
        this.props.navigation.navigate('Search',{keywordName:val});
    } 
    
   
    render(){

        const { data, searchTerm, searchAttribute, ignoreCase } = this.state;
        return(
            <Container>
                <StatusBar hidden/>
                <Header style={styles.header}>
                    <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Search')}>
                        <Row style={{alignItems: 'center'}}>
                            <View style={{paddingRight: 20}}>
                                <FontAwesomeIcon style={{fontSize: 21}} icon={faArrowLeft} />
                            </View>
                            <Col>
                                <Text style={{fontSize: 14}}>Select Keyword</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                </Header>
                <Content>
                    <View style={styles.searchContent}>
                        <Text style={styles.title}>Keyword</Text>
                        <View styles={styles.searchInput}>
                            <TextInput 
                                autoFocus = {true}
                                style={styles.input} 
                                onChangeText={(text) =>{
                                        this.getKey(text),
                                        this.setState({text})
                                    }
                                } 
                                onSubmitEditing={()=>this.submit()}
                            />
                        </View>
                    </View>
                     <SearchableFlatList 
                        style={{paddingHorizontal: 10, paddingTop: 10}} 
                        data={data}
                        searchTerm={searchTerm}
                        searchAttribute={searchAttribute} 
                        ignoreCase={ignoreCase} 
                        renderItem={
                            ({ item }) => (
                                <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('Search',{keywordName:item.name})}>
                                    <Row style={styles.listItemContainer}>
                                        <View style={{paddingRight: 10}}>
                                            <Image
                                              style={{height: 30, width: 30}}
                                               source = {(item.logo_path != '') ? {uri: item.logo_path} : {uri: 'https://www.hotelwaze.com/assets/img/noavailable.jpg'}}
                                            />
                                        </View>
                                        <Col>
                                            <Text style={{fontSize: 14}}>{item.name}</Text>
                                            <Text style={{fontSize: 10, color: '#c1c1c1'}}>{item.address_1}</Text>
                                        </Col>
                                    </Row>
                                </TouchableOpacity>
                            )
                        }
                        keyExtractor={(item) => item.hotel_id.toString()} 
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({    
    container:{
        padding: 10,
    },
    header:{
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 0,
    },
    searchContent:{
        flex: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 20,
        margin: 10,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    title:{
        fontSize: 10,
        color: '#a5a5a5',
        paddingBottom: 5,
    },
    searchInput:{
        backgroundColor: '#f1f1f1',
        position: 'absolute', 
        top: 0,
        left: 0,
        right: 0,
    },
    input:{
        height: 30,
        padding: 10,
        fontSize: 10,
        borderWidth: 1,
        borderRadius: 15,
        borderColor: '#f1f1f1',
        backgroundColor: '#f1f1f1',
    },
    listItemContainer:{
        borderBottomWidth:0.5,
        paddingVertical: 10,
        borderBottomColor: '#f1f1f1',
        justifyContent: 'center'
    }
    
});
import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TextInput, TouchableOpacity, Image, BackHandler} from 'react-native';
import { Container, Title, Header, Row, Col, Content, List, ListItem, Button} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

export default class NoConnectionScreen extends Component{
	componentDidMount() {
    	this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  	}

  	handleBackPress = () => {
	    this.props.navigation.push('Home'); // works best when the goBack is async
	    return true;
	}

	render(){
		return(
			<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
				<StatusBar hidden/>
				<View>
					<Image
			          style={{width: 100, height: 100}}
			          source={require('../assets/no_connection.png')}
			        />
				</View>
				<View>
					<Text>No Internet Connection</Text>
				</View>
			</View>
		);
	}
}


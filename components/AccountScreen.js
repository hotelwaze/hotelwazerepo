import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions,TouchableOpacity, StatusBar, AsyncStorage} from 'react-native';
import {Row, Col, Container, Content, Button, Footer, FooterTab } from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPowerOff, faArrowRight, faHome, faUser, faSearch, faEnvelope } from '@fortawesome/free-solid-svg-icons'

export default class HomeScreen extends Component{

	_handleAccount = () =>{
		console.log(tokenx);
		if(tokenx == ''){
			this.props.navigation.navigate('SignIn')
		}else{
			this.props.navigation.navigate('Account');
		}
	}

	_handleLogout = async() => {
		try {
		  await AsyncStorage.removeItem('_signInSession');
		  const value = await AsyncStorage.getItem('_signInSession');
		  global.tokenx = '';
		  this._out(); 
		} catch (error) {
		  console.log("Error resetting data"+error);
		}
	};

	_out = () =>{
		this.props.navigation.navigate('SignIn');
	}

	render(){
		return(
			<Container style={styles.container}>
				<StatusBar hidden/>
				<Content>
					<View style={styles.contentWrapper}>
						<Row>
							<View style={styles.letterBox}>
								<Text style={styles.firtsLetter}>N</Text>
							</View>
							<Col style={styles.details}>
								<Text style={styles.name}>{firstNamex} {lastNamex}</Text>
								<Text style={styles.email}>Email: {emailx}</Text>
								<Text style={styles.subscriptionDetails}>Subscription: Jan 2, 2019 - Jan 2, 2020</Text>
							</Col>
							<View>
								<Button transparent style={styles.offSign} onPress={this._handleLogout}>
									<FontAwesomeIcon style={styles.icon} icon={ faPowerOff } />
								</Button>
							</View>
						</Row>
						<Row>
							<View style={{flex: 1, paddingTop: 20}}>
								<Text>Account</Text>
								<View style={styles.list}>
									<Button transparent style={styles.listItem} onPress={()=>this.props.navigation.navigate('AccountChangeEmail')}>
										<Row style={styles.listItemContent}>
											<Col>
												<Text style={styles.listItemText}>Change Email</Text>
											</Col>
											<View>
												<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
											</View>
										</Row>
									</Button>
								</View>
								<View style={styles.list}>
									<Button transparent style={styles.listItem} onPress={()=>this.props.navigation.navigate('AccountChangePassword')}>
										<Row style={styles.listItemContent}>
											<Col>
												<Text style={styles.listItemText}>Change Password</Text>
											</Col>
											<View>
												<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
											</View>
										</Row>
									</Button>
								</View>
							</View>
						</Row>
						<Row>
							<View style={{flex: 1, paddingTop: 20}}>
								<Text>Subscription</Text>
								<View style={styles.list}>
									<Button transparent style={styles.listItem} onPress={()=>this.props.navigation.navigate('Reactivation')}>
										<Row style={styles.listItemContent}>
											<Col>
												<Text style={styles.listItemText}>Reactivate Subscription</Text>
											</Col>
											<View>
												<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
											</View>
										</Row>
									</Button>
								</View>
								<View style={styles.list}>
									<Button transparent style={styles.listItem} onPress={()=>this.props.navigation.navigate('CancelSubscription')}>
										<Row style={styles.listItemContent}>
											<Col>
												<Text style={styles.listItemText}>Cancel Subscription</Text>
											</Col>
											<View>
												<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
											</View>
										</Row>
									</Button>
								</View>
								<View style={styles.list}>
									<Button transparent style={styles.listItem} onPress={()=>this.props.navigation.navigate('AccountChangeEmail')}>
										<Row style={styles.listItemContent}>
											<Col>
												<Text style={styles.listItemText}>Transaction History</Text>
											</Col>
											<View>
												<FontAwesomeIcon style={styles.listItemIcon} icon={faArrowRight} />
											</View>
										</Row>
									</Button>
								</View>
							</View>
						</Row>
					</View>
				</Content>
				<Footer>
					<FooterTab style={styles.footerContainer}>
						<Button  onPress={() => this.props.navigation.navigate('Home')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faHome} />
							<Text style={styles.navInactiveText} >Home</Text>
						</Button>
						<Button onPress={() => this.props.navigation.navigate('Search')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faSearch} />
							<Text style={styles.navInactiveText} >Search</Text>
						</Button>
						{/*<Button onPress={() => this._handleAccount()}>
							<FontAwesomeIcon style={styles.navActiveIcon} icon={faUser} />
							<Text style={styles.navActiveText} >Account</Text>
						</Button>*/}
						<Button onPress={() => this.props.navigation.navigate('ContactUs')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faEnvelope} />
							<Text style={styles.navInactiveText} >Contact Us</Text>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentWrapper:{
  	padding: 10,
  	flex: 1,
  	//backgroundColor: 'green'
  },
  letterBox:{
  	width: 46,
  	height: 46,
  	backgroundColor: '#003580',
  	borderRadius: 46,
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  details:{
  	paddingLeft: 10
  },
  firtsLetter:{
  	color: '#fff',
  	fontSize: 21,
  	fontWeight: 'bold'
  },
  name:{
  	fontSize: 18,
  	fontWeight: 'bold',
  	color: '#003580'
  },
  email:{
  	fontSize: 10,
  	color:'#A5A5A5'
  },
  subscriptionDetails:{
  	fontSize: 10
  },
  offSign:{
  	height: 30,
  	width: 30,
  	backgroundColor: '#006CFF',
  	justifyContent: 'center',
  	alignItems: 'center',
  	borderRadius: 30,
  },
  icon:{
  	color: '#fff',
  	fontSize: 20
  },
  list:{
  	paddingTop: 15
  },
  listItem:{
  	paddingHorizontal: 15,
  	paddingVertical: 0,
  	height: 30,
  	backgroundColor: '#fff',
  	borderRadius: 15,
  	shadowColor: "#000",
	shadowOffset: {
	 width: 0,
	 height: 1,
	},
	shadowOpacity: 0.22,
	shadowRadius: 2.22,
	elevation: 3
  },
  listItemContent:{
  	alignItems: 'center'
  },
  listItemText:{
  	fontSize: 14,
  	color: '#A5A5A5'
  },
  listItemIcon:{
  	fontSize: 18,
  	color: '#A5A5A5'
  },
  footerContainer:{
	backgroundColor: '#fff',
	},
	navActiveIcon:{
		fontSize: 20,
		color: '#000066',
	},
	navActiveText:{
		fontSize: 14,
		color: '#000066',
	},
	navInactiveIcon:{
	fontSize: 20,
	color: '#a5a5a5',
	},
	navInactiveText:{
	fontSize: 14,
	color: '#a5a5a5',
	}
});
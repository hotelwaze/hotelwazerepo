import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions,TouchableOpacity, StatusBar, TextInput, Alert} from 'react-native';
import {Row, Col, Container, Content, Button, Footer, FooterTab, Item, Input, Textarea, Form} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {faHome, faUser, faSearch, faEnvelope } from '@fortawesome/free-solid-svg-icons'

export default class ContactUsScreen extends Component{
	constructor(props) {
    	super(props);
  	}

	_handleAccount = () =>{
		if(tokenx == ''){
			this.props.navigation.navigate('SignIn');
		}else{
			this.props.navigation.navigate('Account');
		}
	}

	_sendMessage = () =>{
		//console.log(sendName);
		fetch('http://api.hotelwaze.com/index/contactus', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				apikey: '1',
				name: this._name._lastNativeText,
				email: this._email._lastNativeText,
				subject: this._subject._lastNativeText,
				message: this._message._lastNativeText,
			}),
		})
		.then((response) => response.json())
		.then((responseJson) => {
			console.log(responseJson);
			if(responseJson.code == 0){
				Alert.alert(
					'Message has been sent!',
					'Your message has been sent. We will respont to you as soon as we could.',
					[
					    {text: 'OK', onPress: () => this.props.navigation.navigate('Home')},
					],
					{cancelable: false},
				)
			}else{
				alert(responseJson.msg);
			}
		})
		.catch((error) => {
			console.error(error);
		})
	}


	render(){
		return(
			<Container style={styles.container}>
				<StatusBar hidden/>
				<Content>
					<View style={styles.contentWrapper}>
						<View style={styles.boxShadow}>
							<Text style={{fontSize: 14, fontWeight: 'bold', color:'#000066'}}>We'd Love to hear from You!</Text>
							<Text style={{fontSize:12, paddingBottom: 5, color:'#000066'}}>There's much to see here. So, take your time, look around, and learn all there is to know about us. Protect yourself against fake hotel websites. Your needs and satisfaction are our top priorities.
							</Text>
							<Text style={{fontSize:12}}>
								<Text style={{fontWeight: 'bold', color:'#000066'}}>Address:</Text>
								<Text style={{color:'#000066'}}> Primeraka Travel Company Ice Skate Place,San Diego California 92126, USA</Text>
							</Text>
							<Text style={{fontSize:12}}>
								<Text style={{fontWeight: 'bold', color:'#000066'}}>E-mail:</Text>
								<Text style={{color:'#000066'}}> info@hotelwaze.com</Text>
							</Text>
							<Text style={{fontSize:12}}>
								<Text style={{fontWeight: 'bold', color:'#000066'}}>US Phone:</Text>
								<Text style={{color:'#000066'}}> 1-858-565-6708</Text>
							</Text>
						</View>
						<Form style={{flex: 1}}>
							<View style={{flex: 1, paddingTop: 20}}>
								<Text style={{fontSize: 10, paddingBottom: 3, color: '#a5a5a5'}}>Name<Text style={{color: 'red'}}> *</Text></Text>
								<Item rounded style={{height:30, backgroundColor:'#f1f1f1'}}>
									<TextInput
										style={{paddingHorizontal: 10, height: 30, borderColor: '#f1f1f1', fontSize: 12, width: '100%'}}
										returnKeyType="next"
										onSubmitEditing={()=>this._email.focus()}
										autoCapitalize="none"
										autoCorrect={false}
										ref={_name => this._name = _name} 
					                />
								</Item>
							</View>
							<View style={{flex: 1, paddingTop: 20}}>
								<Text style={{fontSize: 10, paddingBottom: 3, color: '#a5a5a5'}}>Email Address<Text style={{color: 'red'}}> *</Text></Text>
								<Item rounded style={{height:30, backgroundColor:'#f1f1f1'}}>
									<TextInput
										style={{paddingHorizontal: 10, height: 30, borderColor: '#f1f1f1', fontSize: 12, width: '100%'}}
										returnKeyType="next"
										onSubmitEditing={()=>this._subject.focus()}
                  						keyboardType="email-address"
                  						autoCapitalize="none"
										ref={_email => this._email = _email}
					                />
								</Item>
							</View>
							<View style={{flex: 1, paddingTop: 10}}>
								<Text style={{fontSize: 10, paddingBottom: 3, color: '#a5a5a5'}}>Subject<Text style={{color: 'red'}}> *</Text></Text>
								<Item rounded style={{height:30, backgroundColor:'#f1f1f1'}}>
									<TextInput
										style={{paddingHorizontal: 10, height: 30, borderColor: '#f1f1f1', fontSize: 12, width: '100%'}}
					                    returnKeyType="next"
										onSubmitEditing={()=>this._message.focus()}
                  						autoCapitalize="none"
										ref={_subject => this._subject = _subject}
					                />
								</Item>
							</View>
							<View style={{flex: 1, paddingTop: 10}}>
								<Text style={{fontSize: 10, paddingBottom: 3, color: '#a5a5a5'}}>Message<Text style={{color: 'red'}}> *</Text></Text>
								<TextInput
										style={{padding: 10,borderWidth: 0.5, backgroundColor: '#f1f1f1', borderRadius: 5, borderColor: '#f1f1f1', textAlignVertical: 'top', height: 80, width: '100%', fontSize: 12}}
										multiline={true}
    									numberOfLines={5}
					                    returnKeyType="go"
						                ref={_message => this._message = _message} 
					                />
							</View>
							<View style={{flex: 1, paddingTop: 10, alignItems: 'flex-end'}}>
								<TouchableOpacity 
									style={{width: 100, height: 30, borderRadius: 15, backgroundColor:'#006CFF', alignItems: 'center', justifyContent:'center'}} 
									onPress={()=>this._sendMessage()}
								>
									<Text style={{fontSize: 14, fontWeight:'bold', color: '#fff'}}>SEND</Text>
								</TouchableOpacity>
							</View>
						</Form>
					</View>
				</Content>
				<Footer>
					<FooterTab style={styles.footerContainer}>
						<Button  onPress={() => this.props.navigation.push('Home')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faHome} />
							<Text style={styles.navInactiveText}>Home</Text>
						</Button>
						<Button onPress={() => this.props.navigation.push('Search')}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faSearch} />
							<Text style={styles.navInactiveText} >Search</Text>
						</Button>
						{/*<Button onPress={() => this._handleAccount()}>
							<FontAwesomeIcon style={styles.navInactiveIcon} icon={faUser} />
							<Text style={styles.navInactiveText} >Account</Text>
						</Button>*/}
						<Button onPress={() => this.props.navigation.push('ContactUs')}>
							<FontAwesomeIcon style={styles.navActiveIcon} icon={faEnvelope} />
							<Text style={styles.navActiveText} >Contact Us</Text>
						</Button>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  boxShadow:{
  	padding: 10,
  	borderRadius: 5,
	backgroundColor: '#fff',
	shadowColor: "#000",
	shadowOffset: {
		width: 0,
		height: 1,
	},
	shadowOpacity: 0.22,
	shadowRadius: 2.22,
	elevation: 3,
  },
  contentWrapper:{
  	padding: 10,
  	flex: 1,
  	//backgroundColor: 'green'
  },
  footerContainer:{
		backgroundColor: '#fff',
	},
	navActiveIcon:{
		fontSize: 20,
		color: '#000066',
	},
	navActiveText:{
		fontSize: 14,
		color: '#000066',
	},
	navInactiveIcon:{
		fontSize: 20,
		color: '#a5a5a5',
	},
	navInactiveText:{
		fontSize: 14,
		color: '#a5a5a5',
	}
});
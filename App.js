
import React, {Component} from 'react';
//import DrawerNavigator from './components/DrawerItem';
import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator, DrawerNavigatorItems} from 'react-navigation-drawer';
import {StyleSheet, Text, View, ScrollView, Dimensions, Image, TouchableHighlight, Animated, AsyncStorage, Alert, Netinfo, Platform} from 'react-native';
import {Row, Col, Icons, Content, List, ListItem, Button} from 'native-base';
import SafeAreaView from 'react-native-safe-area-view';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPowerOff, faArrowRight } from '@fortawesome/free-solid-svg-icons'

import HomeScreen from './components/HomeScreen';
import SearchScreen from './components/SearchScreen';
import SingleSearchItemScreen from './components/SingleSearchItemScreen';

import SelectKeywordScreen from './components/SelectKeywordScreen';
import SelectCountryScreen from './components/SelectCountryScreen';
import SelectStateScreen from './components/SelectStateScreen';
import SelectCityScreen from './components/SelectCityScreen';

import AccountScreen from './components/AccountScreen';
import ContactUsScreen from './components/ContactUsScreen';

import SignInScreen from './components/SignInScreen';
import RegisterScreen from './components/RegisterScreen';
import PaymentScreen from './components/PaymentScreen';
import Terms from './components/Terms';

import AccountChangePasswordScreen from './components/AccountChangePasswordScreen';
import AccountChangeEmailScreen from './components/AccountChangeEmailScreen';
import TransactionHistoryScreen from './components/TransactionHistoryScreen';

import ReactivationScreen from './components/ReactivationScreen';
import CancelSubscriptionScreen from './components/CancelSubscriptionScreen';

import NoConnectionScreen from './components/NoConnectionScreen';

import DrawerItem from './components/DrawerItem';

import EnquiryScreen from './components/EnquiryScreen';

const StackNavigation = createStackNavigator(
	{
		Home: HomeScreen,
		Search: SearchScreen,
		Account: AccountScreen,
		ContactUs: ContactUsScreen,
		SingleSearchItem: SingleSearchItemScreen,
    SelectKeyword:SelectKeywordScreen,
		SelectCity: SelectCityScreen,
		SelectCountry: SelectCountryScreen,
		SelectState: SelectStateScreen,
		SignIn: SignInScreen,
		Register: RegisterScreen,
		Payment: PaymentScreen,
		Terms: Terms,
    AccountChangePassword: AccountChangePasswordScreen,
    AccountChangeEmail: AccountChangeEmailScreen,
    TransactionHistory: TransactionHistoryScreen,
    Reactivation: ReactivationScreen,
    CancelSubscription: CancelSubscriptionScreen,
    NoConnection: NoConnectionScreen,
    Enquiry: EnquiryScreen
	}, 
	{
    initialRouteName: 'Home',
    headerMode: 'none',
	  navigationOptions: {
	    headerVisible: false,
	  }
  }
 );


const CustomDrawerComponents = ({navigation}) => (
	<DrawerItem navigation={navigation} />
);


const DrawerNav = createDrawerNavigator(
	{
		HomeDrawer: HomeScreen,
		AccountDrawer: AccountScreen
	},
	{
		initialRouteName: 'HomeDrawer',
		drawerWidth: 300,
		drawerPosition: 'left',
		contentComponent: CustomDrawerComponents 
	}
)

const MainNavigation = createSwitchNavigator({
  Main: StackNavigation,
  Drawer: DrawerNav, 
})

const AppContainer = createAppContainer(MainNavigation);

class App extends React.Component {
	constructor() {
	    super();
	    global.tokenx = '18be1a6b81abdd725cc44c5d064f1fe246e2020b';
	}
	render() {
		return (
			<AppContainer
				ref={
					nav => {
						this.navigator = nav;
					}
				}
			/>
		);
	}
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentWrapper:{
  	padding: 10,
  	flex: 1,
  	//backgroundColor: 'green'
  },
  letterBox:{
  	width: 46,
  	height: 46,
  	backgroundColor: '#003580',
  	borderRadius: 46,
  	justifyContent: 'center',
  	alignItems: 'center'
  },
  details:{
  	paddingLeft: 10
  },
  firtsLetter:{
  	color: '#fff',
  	fontSize: 21,
  	fontWeight: 'bold'
  },
  name:{
  	fontSize: 18,
  	fontWeight: 'bold',
  	color: '#003580'
  },
  email:{
  	fontSize: 10,
  	color:'#A5A5A5'
  },
  subscriptionDetails:{
  	fontSize: 10
  },
  offSign:{
  	height: 30,
  	width: 30,
  	backgroundColor: '#006CFF',
  	justifyContent: 'center',
  	alignItems: 'center',
  	borderRadius: 30,
  },
  icon:{
  	color: '#fff',
  	fontSize: 20
  },
  list:{
  	paddingTop: 15
  },
  listItem:{
  	paddingHorizontal: 15,
  	paddingVertical: 0,
  	height: 30,
  	backgroundColor: '#fff',
  	borderRadius: 15,
  	shadowColor: "#000",
	shadowOffset: {
	 width: 0,
	 height: 1,
	},
	shadowOpacity: 0.22,
	shadowRadius: 2.22,
	elevation: 3
  },
  listItemContent:{
  	alignItems: 'center'
  },
  listItemText:{
  	fontSize: 14,
  	color: '#A5A5A5'
  },
  listItemIcon:{
  	fontSize: 18,
  	color: '#A5A5A5'
  }

});